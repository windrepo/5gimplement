package net.wind3.youonetfork.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import androidx.core.app.ActivityCompat;

import android.telephony.CellIdentityNr;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrengthNr;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import net.wind3.youonetfork.MainActivity;
import net.wind3.youonetfork.R;
import net.wind3.youonetfork.task.TaskOkHttp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import fr.bmartel.speedtest.SpeedTestReport;
import fr.bmartel.speedtest.SpeedTestSocket;
import fr.bmartel.speedtest.inter.IRepeatListener;
import fr.bmartel.speedtest.inter.ISpeedTestListener;
import fr.bmartel.speedtest.model.ComputationMethod;
import fr.bmartel.speedtest.model.SpeedTestError;
import fr.bmartel.speedtest.utils.SpeedTestUtils;

import static android.content.ContentValues.TAG;
import static android.net.TrafficStats.getUidRxBytes;
import static java.lang.Thread.sleep;

public class TestService extends Service {


    /* Consts ff */
    public static Context mContext;
    public static final int GPS_TIME_MONITOR = 1000; //was 3s..for battety safety
    public static final int APPEND_TIME = 1000;
    public static final String MASSIVE_URL = "https://youONet.cloud/uploads/100MB.zip";

    /* old tests */
    // public static final String MASSIVE_URL = "http://1.testdebit.info/500M.iso";

    public static final String TPUT_DOWNLOAD_PATH = "/Download";

    //DOWNLOAD TEST monitoring speed
    public static int uid = 0;
    private static long totalReceivedKBytes;
    private static long currentReceivedKBytes;
    public static long startReceivedKBytes;
    public static long lastIdRef;
    public static int indexRef = 0;
    public static int bufferForDelete = 0;
    public static int dlcounter = 0;
    public static long dlSum = 0;
    public static long dlAvr = 0;
    public static long instaspeed = 0;
    public static long maxSpeedDL = 0;
    public static long maxSpeedUL = 0;

    public static long lastDLavr = 0;
    public static long lastULavr = 0;
    public static long lastDropped = 0;
    public static String lastPing = "";

    public static String avrLevelForIDLE = "asdasd";

    public static String urlMap = "http://www.google.com";
    public static String sessionId = "---";
    public static String token = "ffff";
    public static String mail = "";
    public static String pass = "";
    public static boolean isCreated = false;
    public static boolean isAppending = false;
    public static boolean isCellMonitoring = true;
    public static boolean shouldSkip = false;

    //CALL looper sys
    public static boolean shouldCall = false;
    public static boolean falseHook = false;
    public static int dropped = 0;
    private static int counterDropTimer = 0;

    //screen-time sys & test type
    public static int screen = 0;
    public static int testType = 0;
    public static String logType = "";

    public static TelephonyManager tm;
    public static TelephonyManager cm;
    public static LocationManager lmanager;
    public static DownloadManager dm;

    //PING TEST vars
    public static float instaPing = 0;
    public static float sumPing = 0;

    //SPEED TEST vars
    public static int stateForST = 0;
    public static int iterationForST = 0;
    public static int cloneIteration = 0;
    static SpeedTestSocket speedTestSocket;
    public static StringBuffer logSpeedTest;
    public static boolean iAmSpeedTest = false;
    public static long speedSTdl;
    public static long speedSTup;
    public static String pingST = "failed";
    public static String pingAverage = "";
    public static long speedListUP[];
    public static long speedListDL[];
    public static float speedListPing[];
    public static int counterListUP = 0;
    public static int counterListDL = 0;
    public static int counterListPing = 0;
    public static long refSpeed = 0;

    //IDLE average
    public static double idleSum = 0;
    public static int counterIdle = 0;

    //special handler
    static Handler recon;
    static Runnable ret;

    //check upload valide round
    static boolean validateUpload = false;
    static int retryAttempsPing = 0;
    static int retryAttemps = 0;
    static boolean shouldAsk = true;

    //LOG body
    public static String body = "";
    public static String bodyTemp = "";
    public static String bodyTemp1 ="";
    public static String bodyTempNR ="";
    public static String debugBody = "";
    public static String file = "";
    static String myImei = "";

    //Download receiver flag
    public static boolean dlqueueregistered = false;

    //COORDS GPS
    static String lat;
    static String lon;
    public static LocationListener locationListener;

    //Data/Time var
    public static String dateTime = "";

    //MainActivity
    public static MainActivity ptr = null;

    //reading vars for UI toogle
    public static List<CellInfo> cellinfo2;
    public static String cellid[];
    public static String mcc = "0";
    public static String mnc = "0";
    public static String pci = "0";
    public static String prevnci ;
    public static String pciUI ="pci5g";
    public static String tac = "0";
    public static String freq = "0";
    public static String earfnc = "0";
    public static String rsrp3 = "0";
    public static String rsrq3 = "0";
    public static String cqi = "0";
    public static String rssnr = "0";
    public static String ci = "0";
    public static String pci5G = "0";
    public static String rat = "0";
    public static String rat2 = "0";
    public static String enb = "0";
    public static String worked = "0";
    public static String registerStatus = "0";
    public static String tacUI = tac;
    public static String freqUI ;
    public static String rsrp3UI ="rsrp";
    public static String rsrq3UI="rsrq" ;
    public static String ciUI = ci;
    public static String enbUI = enb;
    public static String ratUI = rat;

    protected SignalStrengthStateListener mSignalStrengthStateListener;
    public SignalStrength mSignalStrength;

    public static SimplePhoneStateListener phoneListener;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("lifesaver", "passed");
        Log.d("lifesaver", "cvs");
        startForeground(this);
        resetValues();
        startMonitor();

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }

    private void startForeground(Service service) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Sets an ID for the notification, so it can be updated.
            int notifyID = 1;
            String CHANNEL_ID = "my_channel_01";// The id of the channel.
            CharSequence name = "youONet is activated";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            // Create a notification and set the notification channel.
            Notification notification = new Notification.Builder(service, CHANNEL_ID)
                    .setContentTitle("youONet" + " reminder :")
                    .setContentText("youONet is active in background..")
                    .setSmallIcon(R.drawable.service)
                    .setChannelId(CHANNEL_ID)
                    .build();

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);

            // Issue the notification.
            mNotificationManager.notify(notifyID, notification);
            startForeground(notifyID, notification);
        } else {
            Notification notification = new Notification.Builder(service)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(getString(R.string.app_name) + " Notification system :")
                    .setContentText("Background service is up and running")
                    .setSmallIcon(R.drawable.service)
                    .build();
            startForeground(1, notification);
        }


        Log.d("SERVICE", "Starting from Service");
        Log.d("SERVICE", "Starting from Service");

    }

    public void resetValues() {
        isCreated = true;
        isCellMonitoring = true;
        isAppending = false;
        shouldCall = false;
        falseHook = false;
        dropped = 0;
        screen = 0;
        myImei = "";
        mContext = getApplicationContext();
        Log.d("SERVICE", "reset" + screen);

        lastIdRef = 0;
        indexRef = 0;
        uid = 0;
        totalReceivedKBytes = 0;
        currentReceivedKBytes = 0;
        startReceivedKBytes = 0;
        bufferForDelete = 0;
        dlcounter = 0;
        dlSum = 0;
        maxSpeedDL = 0;
        maxSpeedUL = 0;
        speedSTdl = 0;
        speedSTup = 0;
        refSpeed = 0;
        instaPing = 0;

        lastDLavr = 0;
        lastULavr = 0;
        lastDropped = 0;
        lastPing = "";

        stateForST = 0;
        iterationForST = 1;

        dlqueueregistered = false;

        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Log.d("blocco", "ho appena riletto tm");
        //ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        lmanager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        dm = (DownloadManager) mContext.getSystemService(DOWNLOAD_SERVICE);

        mSignalStrengthStateListener = new SignalStrengthStateListener();
        tm.listen(mSignalStrengthStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS | PhoneStateListener.LISTEN_CELL_LOCATION);
    }

    public void startMonitor() {
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                try {
                    goforReading();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                if (isCellMonitoring) handler.postDelayed(this, 1000);
                else handler.removeCallbacksAndMessages(null);
            }
        };

        if (isCellMonitoring) handler.postDelayed(runnable, 1000);

    }


    public void goforReading() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Log.d("ericsson", "dentro goforreading");
        /* unrolled test */
        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }

        //  Log.d("valoriNR", "capa" + capa);
        int networkType = tm.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
                rat = "2";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                rat = "3";
                break;
            case TelephonyManager.NETWORK_TYPE_NR:
                rat = "5";
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                rat = "4";
                break;
            default:
                rat = "0";
        }


        /*int networkType2 = cm.getNetworkType();
        switch (networkType2) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
                rat2 = "2";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                rat2 = "3";
                break;
            case TelephonyManager.NETWORK_TYPE_NR:
                rat2 = "5";
                break;

            case TelephonyManager.NETWORK_TYPE_LTE:
                rat2 = "4";
                break;


            default:
                rat2 = "0";
        }
*/


        //Log.d("valoriNR", "rat2: " + rat2);

        if ( !lmanager.isProviderEnabled( LocationManager.GPS_PROVIDER ) && mSignalStrength==null) {
            Log.d("SERVICE", "GPS is not active");

        }

        else {

            List<CellInfo> cellInfoList = tm.getAllCellInfo();


            Log.d("blocco", "ho appena riletto cell info");
            int index = 0;
            String ind = "";

            /*List <CellInfo> allCellInfo2 = tm.getAllCellInfo();

            for (CellInfo cellInfo2 : allCellInfo2) {
                if (cellInfo2 instanceof CellInfoNr && cellInfo2.getCellConnectionStatus() == CellInfo.CONNECTION_PRIMARY_SERVING) {
                   // isConnectedToStandalone5GNetwork = true;
                    Log.d("valoriNR", "dentro il nuovo cell info " )
                    break;
                }
            }
*/
            int tmpRsrp = 0;

            if (cellInfoList == null) Log.d("valoriNR", "cell info list Ã¨ vuoto " );
            if (cellInfoList != null) {
                // Log.d("ericsson", "dentro CELLINFOLIST NOT NULL");

                //
                //
                //    return;
                //}dangerous! cell MUST not be null.

                //LTE
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }



                bodyTempNR="";



                Log.d("ericssonn", "cellinfolist " + cellInfoList);
                bodyTemp = "";
                Log.d("idrovore", "che cella sento fuori " + cellInfoList);
                for (CellInfo cellInfo : cellInfoList) {
                    Log.d("idrovore", "che cella sento ? " + cellInfoList);

                    //qui inizia if NR



                    if (cellInfo instanceof CellInfoNr) {
                        //Log.d("valoriNR", "prima di registered nr ");

                        //if (cellInfo.isRegistered()) {
                        if (1>0) {
                            final CellSignalStrengthNr nrsignal = (CellSignalStrengthNr) ((CellInfoNr) cellInfo).getCellSignalStrength();
                            final CellIdentityNr nridentity = (CellIdentityNr) ((CellInfoNr) cellInfo).getCellIdentity();
                            String csirsrp = nrsignal.getCsiRsrp()+ "";
                            String getasulevel = nrsignal.getAsuLevel()+ "";
                            String csirsrq = nrsignal.getCsiRsrq()+ "";
                            String csisinr = nrsignal.getCsiSinr()+ "";
                            String getdbm = nrsignal.getDbm()+ "";
                            String getlevel = nrsignal.getLevel()+ "";
                            //valori temp UI5G
                            String ssrsrp = "rsrp"+ "";
                            String ssrsrq = "rsrq"+ "";
                            String pci5g = "5gpci"+ "";

                            //valori temp UI5G


                            //valori reali UI5G
                            /*String ssrsrp = nrsignal.getSsRsrp()+ "";
                            String ssrsrq = nrsignal.getSsRsrq()+ "";
                            String pci = nridentity.getPci()+ "";*/

                            //valori reali UI5G
                            String sssinr = nrsignal.getSsSinr()+ "";

                            String nci = nridentity.getNci()+ "";
                            String nrarfcn = nridentity.getNrarfcn()+ "";

                            tac = nridentity.getTac()+ "";


                            mcc = nridentity.getMccString()+ "";
                            mnc = nridentity.getMncString()+ "";
                            Log.d("idrovoreNR", "csirsrp "+csirsrp +"getasulevel "+ getasulevel + " csirsrq" + csirsrq + "csisinr " + csisinr + "getdbm "
                                    + getdbm +"getlevel "+getlevel+ "ssrsrp "+ssrsrp + "ssrsrq " + ssrsrq+"sssinr "+sssinr+"nci "+nci+"nrarfcn "+nrarfcn+"pci "+pci5g);

                            if (isAppending){
                                if (cellInfo.isRegistered()) {
                                    // Log.d ("datetime" , ""+ dateTime);
                                    registerStatus = "true";
                                    if (dateTime == "") dateTime = getLogsDate();
                                    logType =  "IDLE_LOG";
                                    bodyTemp += dateTime+" "+pci5g+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc + " "+logType +" "+ enb;
                                    Log.d ("valorilog" , ""+ bodyTemp);
                                    //pciUI = pci5g;
                                    tacUI = tac;
                                    freqUI = freq;
                                    rsrp3UI = rsrp3;
                                    ratUI = rat;
                                    ciUI = ci;
                                    rsrq3UI = rsrq3;
                                    enbUI = enb;
                                }
                                else {logType =  "NBG_NR";
                                    Log.d("bodytempnr", " prevnci " + prevnci + "nci"+ pci);
                                    if (pci5g != prevnci) {
                                        Log.d("bodytempnr", "dentro if");

                                        bodyTempNR+= " | " + csirsrp + " " + getasulevel + " " + csirsrq + " " + csisinr + " "
                                                + getdbm + " " + getlevel + " " + ssrsrp + " " + ssrsrq + " " + sssinr + " " + nci + " " + nrarfcn + " " + pci5g +" "+ logType + " ";
                                        Log.d("bodytempnr", "" + bodyTempNR);



                                        prevnci = pci5g;
                                    }
                                    // registerStatus = "false";
                                }
                            }




                            //nel if qui sotto ci entreremmo sempre quindi non ha senso popolarlo. allo stesso modo non serve else
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                               /* rsrp3 = ((CellInfoNr) cellInfo).getCellSignalStrength().getCsiRsrp() + "";
                                rsrq3 = ((CellInfoNr) cellInfo).getCellSignalStrength().getCsiRsrq() + "";
                                rssnr = ((CellInfoNr) cellInfo).getCellSignalStrength().getCsiSinr() + "";*/
                                // V1 IL CQI NON SEMBRA PROPRIO PRESENTE cqi = ((CellInfoNr) cellInfo).getCellSignalStrength().getCqi() + "";



                            }

                            else {/*

                                //V1 al momento non dovremmo entrare qui
                                rsrp3 = ((CellInfoNr) cellInfo).getCellSignalStrength().getDbm()+"";
                                try {
                                   if (mSignalStrength != null) {

                                        Method method = SignalStrength.class.getDeclaredMethod("getLteCqi");
                                        if (method != null)
                                            cqi = (Integer) method.invoke(mSignalStrength)+"";

                                        Method method2 = SignalStrength.class.getDeclaredMethod("getLteRsrq");
                                        if (method2 != null)
                                            rsrq3 = (Integer) method2.invoke(mSignalStrength)+"";

                                        Method method3 = SignalStrength.class.getDeclaredMethod("getLteRssnr");
                                        if (method3 != null)
                                            rssnr = (Integer) method3.invoke(mSignalStrength)+"";
                                    }
                                } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                                    //dummy
                                }
                           */
                            }  //fine else
/*
                            String ciBinary = Integer.toBinaryString(((CellInfoLte) cellInfo).getCellIdentity().getCi());

                            if (ciBinary.length() < 28)
                                ciBinary = ("0000000000000000000000000000" + ciBinary).substring(ciBinary.length());

                            String enbString = ciBinary.substring(0, ciBinary.length() - 8);
                            enb = Integer.parseInt(enbString, 2) + "";

                            String cellIdString = ciBinary.substring(20, ciBinary.length());
                            worked = Integer.parseInt(cellIdString, 2) + "";
                          //  Log.d("valorinr", "csirsrp:"+ csirsrp +"getasulevel:"+ getasulevel +"csirsrq:"+ csirsrq +"csisinr:"+ csisinr +"getdbm:"+ getdbm +"getlevel:"+ getlevel +"ssrsrp:"+ ssrsrp +"ssrsrq:"+ ssrsrq +"sssinr:"+ sssinr +"nci:"+ nci + "nrarfcn:"+ nrarfcn +"pci:"+ pci +"tac:"+ tac +"mcc:"+mcc +"mnc:"+ mnc + "rat:"+ this.rat);
*/
//
                        }//fine if registered*/
                    }

                    //qui inizia if LTE
                    if (cellInfo instanceof CellInfoLte) {
                        // Log.d("valoriNR", "sto in lte ");

                        cellinfo2 =cellInfoList;
//                        if (cellInfo.isRegistered()) {

                        if (1>0) {
                            registerStatus = cellInfo.isRegistered() + "";
                            mcc = ((CellInfoLte) cellInfo).getCellIdentity().getMcc() + "";
                            mnc = ((CellInfoLte) cellInfo).getCellIdentity().getMnc() + "";
                            tac = ((CellInfoLte) cellInfo).getCellIdentity().getTac() + "";

                            ci = ((CellInfoLte) cellInfo).getCellIdentity().getCi() + "";
                            Log.d("readddd",ci);
                            pci = ((CellInfoLte) cellInfo).getCellIdentity().getPci() + "";
                            earfnc = ((CellInfoLte) cellInfo).getCellIdentity().getEarfcn() + "";
                            Log.d("valoriNR", "prova registration status-" + registerStatus);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                rsrp3 = ((CellInfoLte) cellInfo).getCellSignalStrength().getRsrp() + "";
                                rsrq3 = ((CellInfoLte) cellInfo).getCellSignalStrength().getRsrq() + "";
                                rssnr = ((CellInfoLte) cellInfo).getCellSignalStrength().getRssnr() + "";
                                cqi = ((CellInfoLte) cellInfo).getCellSignalStrength().getCqi() + "";
                            }

                            else {
                                rsrp3 = ((CellInfoLte) cellInfo).getCellSignalStrength().getDbm()+"";
                                try {
                                    if (mSignalStrength != null) {

                                        Method method = SignalStrength.class.getDeclaredMethod("getLteCqi");
                                        if (method != null)
                                            cqi = (Integer) method.invoke(mSignalStrength)+"";

                                        Method method2 = SignalStrength.class.getDeclaredMethod("getLteRsrq");
                                        if (method2 != null)
                                            rsrq3 = (Integer) method2.invoke(mSignalStrength)+"";

                                        Method method3 = SignalStrength.class.getDeclaredMethod("getLteRssnr");
                                        if (method3 != null)
                                            rssnr = (Integer) method3.invoke(mSignalStrength)+"";
                                    }
                                } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                                    //dummy
                                }
                            }

                            String ciBinary = Integer.toBinaryString(((CellInfoLte) cellInfo).getCellIdentity().getCi());

                            if (ciBinary.length() < 28)
                                ciBinary = ("0000000000000000000000000000" + ciBinary).substring(ciBinary.length());

                            String enbString = ciBinary.substring(0, ciBinary.length() - 8);
                            enb = Integer.parseInt(enbString, 2) + "";

                            String cellIdString = ciBinary.substring(20, ciBinary.length());
                            worked = Integer.parseInt(cellIdString, 2) + "";

                            Log.d("cell", mcc + "\n" + mnc + "\n" + tac + " " + pci + " " + ci + " " + this.rat + " " + earfnc + " " + rsrp3 + " " + rsrq3 + " " + rssnr + " " + cqi);

                        }
                        if (isAppending){
                            if (cellInfo.isRegistered()) {
                                // Log.d ("datetime" , ""+ dateTime);
                                registerStatus = "true";
                                if (dateTime == "") dateTime = getLogsDate();
                                logType =  "IDLE_LOG";
                                bodyTemp += dateTime+" "+pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc + " "+logType +" "+ enb;
                                Log.d ("valorilog" , ""+ bodyTemp);
                                //pciUI = pci;
                                tacUI = tac;
                                freqUI = freq;
                                rsrp3UI = rsrp3;
                                ratUI = rat;
                                ciUI = ci;
                                rsrq3UI = rsrq3;
                                enbUI = enb;
                            }
                            else {logType =  "NBG_LTE";
                                bodyTemp += " | " + pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc+ " "+logType +" "+ enb;
                                // registerStatus = "false";
                            }
                            // bodyTemp += bodyTemp1 + bodyTemp2;
                        }
                    }

                    //qui finisce if LTE

                    else if (cellInfo instanceof CellInfoWcdma) {
                        if (cellInfo.isRegistered()) {

                            mcc = ((CellInfoWcdma) cellInfo).getCellIdentity().getMcc() + "";
                            mnc = ((CellInfoWcdma) cellInfo).getCellIdentity().getMnc() + "";
                            tac = ((CellInfoWcdma) cellInfo).getCellIdentity().getLac() + "";

                            pci = ((CellInfoWcdma) cellInfo).getCellIdentity().getPsc() + "";
                            earfnc = ((CellInfoWcdma) cellInfo).getCellIdentity().getUarfcn() + "";


                            try {
                                Method method = SignalStrength.class.getDeclaredMethod("getDbm");
                                if (method != null && mSignalStrength != null) {
                                    tmpRsrp = (Integer) method.invoke(mSignalStrength);
                                    rsrp3 = tmpRsrp + "";
                                }
                            } catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
                                Log.e("mer", "problem");
                            }

                            int dBm = ((CellInfoWcdma) cellInfo).getCellSignalStrength().getDbm();

                            rsrq3 = (tmpRsrp - dBm) + "";

                            rssnr = "0";

                            int ciJ = ((CellInfoWcdma) cellInfo).getCellIdentity().getCid();
                            String ciBinary = Integer.toBinaryString(ciJ);

                            if (ciBinary.length() < 28)
                                ciBinary = ("0000000000000000000000000000" + ciBinary).substring(ciBinary.length());

                            String enbString = ciBinary.substring(0, ciBinary.length() - 16);
                            enb = Integer.parseInt(enbString, 2) + "";


                            String cellIdString = ciBinary.substring(12, ciBinary.length());
                            ci = Integer.parseInt(cellIdString, 2) + "";

                            cqi = "0";

                            Log.d("mauro", mcc + "\n" + mnc + "\n" + tac + " " + pci + " " + ci + " " + rat + " " + earfnc + " " + rsrp3 + " " + rsrq3 + " " + rssnr + " " + cqi);

                        }
                    } else if (cellInfo instanceof CellInfoGsm) {
                        Log.d("valoriNR", "sto dentro gsm");

                        if (cellInfo.isRegistered()) {

                            mcc = ((CellInfoGsm) cellInfo).getCellIdentity().getMcc() + "";
                            mnc = ((CellInfoGsm) cellInfo).getCellIdentity().getMnc() + "";
                            tac = ((CellInfoGsm) cellInfo).getCellIdentity().getLac() + "";

                            ci = (((CellInfoGsm) cellInfo).getCellIdentity().getCid() & 0xffff) + "";
                            pci = ((CellInfoGsm) cellInfo).getCellIdentity().getBsic() + "";
                            earfnc = ((CellInfoGsm) cellInfo).getCellIdentity().getArfcn() + "";

                            enb = "0";

                            try {
                                Method method = SignalStrength.class.getDeclaredMethod("getGsmDbm");
                                if (method != null && mSignalStrength != null) {
                                    tmpRsrp = (Integer) method.invoke(mSignalStrength);
                                    rsrp3 = tmpRsrp + "";
                                    Log.d("helping", rsrp3);
                                } else {
                                    Log.d("helping", "shiiitt");
                                }

                            } catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
                                Log.e("mersss", "problem");
                            }

                            rsrq3 = "0";

                            rssnr = "0";

                            cqi = "0";

                            Log.d("mauro", mcc + "\n" + mnc + "\n" + tac + " " + pci + " " + ci + " " + rat + " " + earfnc + " " + rsrp3 + " " + rsrq3 + " " + rssnr + " " + cqi);

                        }
                    }
                }

                if(!earfnc.equals("")) {
                    freq = getFrequencyByRFCN(earfnc) + "";
                }

                filterZeroRat();

            } //..gps is active.

        } //..cell is not null

    }


    protected void filterZeroRat(){
        if(rat.equals("0")) {
            mcc = "0";
            mnc = "0";
            tac = "0";
            pci = "0";
            ci = "0";
            enb = "0";
            earfnc = "0";
            rsrp3 = "0";
            rsrq3 = "0";
            rssnr = "0";
            cqi = "0";
        }
    }

    protected class SignalStrengthStateListener extends PhoneStateListener {

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            mSignalStrength = signalStrength;
        }


        @Override
        public void onCellLocationChanged(CellLocation location) {
            super.onCellLocationChanged(location);

        }
    }

    public static void stop() {
        isCellMonitoring = false;
        isCreated = false;
        screen=0;
        shouldSkip = false;
        token = "";
    }



    public static void startGps(){

        lmanager = (LocationManager) ptr.getSystemService(Context.LOCATION_SERVICE);

        //first location from sys
        Location location;
        location = getLastKnownLocationFromLocationManager();

        lat = "null";
        lon = "null";

        if (location != null) {
            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
        }

        if (ActivityCompat.checkSelfPermission(ptr, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ptr, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            debugBody += "Permessi GPS mancanti!\n";
            return;
        }

        else {
            lmanager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_TIME_MONITOR, 25, locationListener = new LocationListener() {
                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }
                @Override
                public void onProviderEnabled(String provider) {
                }
                @Override
                public void onProviderDisabled(String provider) {
                }
                @Override
                public void onLocationChanged(final Location location) {
                    lat = String.valueOf(location.getLatitude());
                    lon = String.valueOf(location.getLongitude());
                }
            }, Looper.getMainLooper());


        }
    }


    public static Location getLastKnownLocationFromLocationManager() {

        if (ActivityCompat.checkSelfPermission(ptr, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ptr, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        Location location = null;

        try {
            lmanager = (LocationManager) ptr.getSystemService(Context.LOCATION_SERVICE);

            if (lmanager != null) {

                if (lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    location = lmanager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                } else if (lmanager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    location = lmanager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
            }
        } catch (Exception e) {
            debugBody += "Eccezione I/O durante il tracciamento GPS\n";
            Log.d("SERVICE","exception in GPS");
        }

        return location;
    }



    public static void stopGps(){
        lmanager.removeUpdates(locationListener);
    }

    //Date for Logs
    public static String getLogsDate() {
        return formatDateToString(new Date());
    }

    public static String formatDateToString(Date dateToFormat) {
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return df.format(dateToFormat);
    }
    //-------------


    public static void appendIdleData() {

        idleSum = Math.pow(10,Double.valueOf(rsrp3)/10) + idleSum;
        counterIdle++;
        body+= bodyTemp +"";
        body+= bodyTempNR + "\n";


        //body += dateTime+" "+pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc+" "+"IDLE_LOG"+" "+enb+" "+registerStatus +"\n";

        //body += cellinfo2; "


        // body += cellinfo2+ "\n";


        Log.d("print",body);

    }


    public static void appendMultirabData() {

        if(isAppending) {
            counterDropTimer++;
            monitorSpeedTestDownload(uid);
            body += dateTime + " " + pci + " " + tac + " " + freq + " " + rsrp3 + " " + rat + " " + lat + " " + lon + " " + instaspeed + " " + ci + " " + earfnc + " " + rssnr + " " + cqi + " " + rsrq3 + " " + mcc + " " + mnc + " " + "DROP_LOG" + " " + enb + "\n";
            body+= bodyTempNR + "\n";
        }
    }


    public static void appendDLData() {

        monitorSpeedTestDownload(uid);
        body += dateTime+" "+pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc+" "+"DATA_DL"+" "+enb+"\n";
        body+= bodyTempNR + "\n";
    }

    public static void appendULData() {

        if(ptr!=null) {
            ptr.speedView.speedTo(refSpeed);
        }

        speedListUP[counterListUP] = instaspeed;
        counterListUP++;

        body += dateTime+" "+pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc+" "+"DATA_UL"+" "+enb+"\n";

    }

    public static void appendSTData() {

        if(stateForST==0 || stateForST==1 || stateForST==2) {

            monitorSpeedTestDownload(uid);
            body += dateTime+" "+pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc+" "+"SPEEDTEST_DL"+" "+enb+"\n";

        }

        if(stateForST==3){

            instaspeed = 0;

            if(ptr!=null) {
                ptr.speedView.speedTo(0);
            }
        }

        if(stateForST==4){
            if(ptr!=null) {
                speedListUP[counterListUP] = instaspeed;
                counterListUP++;
                body += dateTime+" "+pci+" "+tac+" "+freq+" "+rsrp3+" "+rat+" "+lat+" "+lon+" "+instaspeed+" "+ci+" "+earfnc+" "+rssnr+" "+cqi+" "+rsrq3+" "+mcc+" "+mnc+" "+"SPEEDTEST_UL"+" "+enb+"\n";
                ptr.speedView.speedTo(refSpeed);
            }
        }


    }

    public static void appendCallData() {
        if(isAppending) {
            counterDropTimer++;
            body += dateTime + " " + pci + " " + tac + " " + freq + " " + rsrp3 + " " + rat + " " + lat + " " + lon + " " + instaspeed + " " + ci + " " + earfnc + " " + rssnr + " " + cqi + " " + rsrq3 + " " + mcc + " " + mnc + " " + "DROP_LOG" + " " + enb + "\n";
        }
    }


    public static void appendPingData(){

        sumPing += instaPing;
        counterListPing++;
        body += dateTime + " " + pci + " " + tac + " " + freq + " " + rsrp3 + " " + rat + " " + lat + " " + lon + " " + instaPing + " " + ci + " " + earfnc + " " + rssnr + " " + cqi + " " + rsrq3 + " " + mcc + " " + mnc + " " + "PING_LOG" + " " + enb + "\n";
    }

    public static void appendForTestType(int type) {
        switch (type) {
            case 0 : {break;}
            case 1 : { appendIdleData();break;}
            case 2 : { appendDLData(); break;}
            case 3 : { appendULData(); break;}
            case 4 : { appendSTData(); break;}
            case 5 : { appendCallData(); break;}
            case 6 : { pingLooper(); appendPingData(); break; }
            case 7 : { appendMultirabData(); break; }
        }
    }


    public static void startAppending() {

        //Looper! @_@
        isAppending = true;

        //gps start
        startGps();

        //DL Test
        if(testType==2){
            startDownload();
        }

        //UL Test
        if(testType==3){
            startUpload();
        }

        if(testType==4){
            semaphoreForST();
        }

        //Set the call looper
        if(testType==5){
            initCaller();
        }

        //Set the Multirab looper
        if(testType==7){
            initCaller();
            startDownload();
        }


        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                //let's try to bind the reports for stabiltiy
                if(isAppending) {
                    dateTime = getLogsDate();
                    appendForTestType(testType);
                }

                if (isAppending) {handler.postDelayed(this, APPEND_TIME);}
                else { ptr.speedView.speedTo(0); handler.removeCallbacksAndMessages(null);}
            }
        };

        if (isAppending) handler.postDelayed(runnable, APPEND_TIME);
    }


    public static void stopAppending(){

        isAppending = false;


        if(testType==7){
            counterDropTimer = 0;
            shouldCall = false;
            killCall();
            tm.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
            falseHook = false;
            lastDropped = dropped;


            removeDownloadZombies();
            bufferForDelete = 0;
            if(dlcounter!=0){
                dlAvr = dlSum/dlcounter;
                lastDLavr = dlAvr;
            }
            else {
                dlAvr = 0;
                lastDLavr = 0;
            }
            deleteAllFiles();
        }

        if(testType==5) {
            counterDropTimer = 0;
            shouldCall = false;
            killCall();
            tm.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
            falseHook = false;
            lastDropped = dropped;
        }

        if(testType==2){
            removeDownloadZombies();
            bufferForDelete = 0;
            if(dlcounter!=0){
                dlAvr = dlSum/dlcounter;
                lastDLavr = dlAvr;
            }
            else {
                dlAvr = 0;
                lastDLavr = 0;
            }
            deleteAllFiles();
        }

        if(testType==4){
            //no-ops
        }

        if(testType==6){
            pingAverage = (sumPing/counterListPing) + "";
        }

        stopGps();
        prepareToSend();
    }



    @SuppressLint("MissingPermission")
    public static void prepareToSend() {
        Log.d("SERVER",body);
        file = saveTestInLogFile(body);
        Log.d("imeiQ"," in prepare to send"+myImei);
        /* Becareful, this will NOT work on Android Q */
        getImei();

        //sending file...
        TaskOkHttp connectionSocket;
        connectionSocket = new TaskOkHttp();
        connectionSocket.setType(1);
        connectionSocket.initVars(ptr,myImei,file);
        connectionSocket.start();

        //.. tell UI that we're good.
        //.nowFinished();

    }


    @SuppressLint("MissingPermission")
    public static void getImei() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            // only for gingerbread and newer versions
            myImei = "111111111111111";
            Log.d("imeiQ"," dentro if getimei"+myImei);

            //myImei = tm.getImei();
        }

        else myImei = "111111111111111";
    }

    /* Android Q getDeviceId, (useless) reflection attemp */
    @SuppressLint("MissingPermission")
    public static void getImeiQ() {
        tm = (TelephonyManager) mContext.getSystemService(TELEPHONY_SERVICE);

        try {
            Class classTelephony;
            classTelephony = Class.forName(tm.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            methodGetITelephony.setAccessible(true);

            Object telephonyInterface = methodGetITelephony.invoke(tm);

            Class telephonyInterfaceClass;
            telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());

            Method methodGetITelephony2 = telephonyInterfaceClass.getDeclaredMethod("getDeviceId",String.class);

            methodGetITelephony2.setAccessible(true);

            myImei = methodGetITelephony2.invoke(telephonyInterface,mContext.getPackageName()) + "";
            Log.d("imeiQ",myImei+" dentro qetimeiq");
        }
        catch (Exception e){
            Log.e("imeiQ",e.toString()+" empty");
        }
    }

    public static String saveTestInLogFile(String bodyL) {
        String filename = "log.txt";

        //String encBody = encrypt(body);
        File directory = ptr.getApplicationContext().getExternalFilesDir(null);

        try {
            File file = new File(directory, filename);

            FileOutputStream fos = new FileOutputStream(file);

            fos.write(bodyL.getBytes());
            fos.flush();
            fos.close();

            Log.d("LOG", "FILE DI LOG SALVATO CORRETTAMENTE");
            Log.d("LOG", filename);

        } catch (Exception e) {
            Log.e("LOG", e.getMessage());
        }
        return filename;
    }


    private static boolean killCall() {
        try {
            body += dateTime+" Call killed";
            Log.d("CALLPHONE","kill call called..ed ed.");
            tm = (TelephonyManager) mContext.getSystemService(TELEPHONY_SERVICE);


            Class classTelephony;
            classTelephony = Class.forName(tm.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            methodGetITelephony.setAccessible(true);


            Object telephonyInterface = methodGetITelephony.invoke(tm);

            Class telephonyInterfaceClass;
            telephonyInterfaceClass = Class.forName(telephonyInterface.getClass().getName());

            Method methodGetITelephony2 = telephonyInterfaceClass.getMethod("endCall");
            methodGetITelephony2.setAccessible(true);
            methodGetITelephony2.invoke(telephonyInterface);

        } catch (Exception ex) {
            /* Dangerous, this is an old attemp to disconnect call for
             * no-reflection-working device, by firing magic number via pid shell */
            // Executor eS = Executors.newSingleThreadExecutor();
            // eS.execute(new Runnable() {
            //     @Override
            //     public void run() {
            //        disconnectCall();
            //    }
            //});

            Log.e("CALLPHONE", ex.getMessage()+"");
            return true;
        }
        return true;


    }


    public static void initPing(){

    }

    public static void initCaller() {

        //First call
        makeCall();

        shouldCall = true;

        phoneListener = new SimplePhoneStateListener();
        tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

    }


    public static int disconnectCall()
    {
        String[] cmdline = {""};
        Runtime runtime = Runtime.getRuntime();
        Log.d("huawow",runtime+"");
        int nResp = 0;
        try
        {
            /* This is a collection of service params for killing radio, firing headset close button etc.. */
            //------CMD SUITE------
            // runtime.exec("service call phone 3 \n");
            //  Process proc = runtime.exec("service call phone 3 \n");

            //  Process proc=  Runtime.getRuntime().exec(new String[] {"su", "-c", "input keyevent 5"});
            // Process proc = runtime.exec("input keyevent 6 \n");

            //  Process proc = runtime.exec("service call phone 3 \n");

            //runtime.exec("input motionevent 720 20 down \n");
            //  Process proc = runtime.exec("input motionevent 720 20 up \n");

            Process proc = runtime.exec("service call phone 3 \n"); //28 lg

            //LOG MESSAGE (P_P)
            //String result = convertStreamToString(proc.getInputStream());
            //Log.d("huawow",  result+"mad");

        } catch(Exception exc)
        {
            Log.d("CALL", "exception \n");
            exc.printStackTrace();
        }
        return nResp;
    }




    public static class SimplePhoneStateListener extends PhoneStateListener {

        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    if(falseHook) {
                        if (shouldCall) {
                            if(counterDropTimer>5) {
                                dropped++;
                                counterDropTimer = 0;
                                //just for drop.. if events are faster than loop
                                /* safe..no need to use semaphores or java monitor obj */
                                dateTime = getLogsDate();
                                body += dateTime + " Dropped call\n";
                                Log.d("CALLPHONE", "Dropped call :" + dropped);
                                makeCall();
                            } else {
                                dateTime = getLogsDate();
                                body += dateTime + " Failed call\n";
                                Log.d("CALLPHONE", "Dropped call...but let's ignore it");
                                makeCall();
                            }
                        }
                    }
                    falseHook = true;
                    Log.d("CALLSTATUS", "IDLE");
                    break;

                case TelephonyManager.CALL_STATE_OFFHOOK:

                    Log.d("CALLSTATUS", "OFFHOOK");
                    break;

                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d("CALLSTATUS", "RINGING");

                    break;
            }
        }

    }

    public static void showAlertSkype(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ptr, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage("Attention : SKYPE has been detected on this phone\nyou will be prompted to select default dialer for calls\nPlease, selelect PHONE DIALER instead of SKYPE to continue")
                .setCancelable(false)
                .setPositiveButton("Ok, got it", new DialogInterface.OnClickListener() {
                    @SuppressLint("MissingPermission")
                    public void onClick(DialogInterface dialog, int id) {

                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + "+390810343062"));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                });

        alertDialogBuilder.show();
    }

    public static boolean getDialerAndFlushCall() {

        int skypeInstancesFound = 0;

        PackageManager pm = mContext.getPackageManager();

        if(pm!=null){
            skypeInstancesFound += checkSkypePkg(pm,"com.skype.insiders");
            skypeInstancesFound += checkSkypePkg(pm,"com.skype.raider");
        }

        if(skypeInstancesFound!=0){
            return true;
        }

        return false;
    }


    public static int checkSkypePkg(PackageManager pm, String path)  {
        /**
         * state var for stages :
         * 1 : package is found
         * 0 : package not found
         */
        int state = 0;
        try {
            pm.getPackageInfo(path, PackageManager.GET_ACTIVITIES);
            return state = 1;
        } catch (PackageManager.NameNotFoundException e) {
            return state = 0;
        }

    }

    @SuppressLint("MissingPermission")
    public static void makeCall(){

        /* if provider is not WIND3, do not make a call */
        boolean isBinded = ((mnc.equals("88") && mcc.equals("222")) ? true : false);

        if(isBinded) {

            dateTime = getLogsDate();
            body += dateTime + " Setup call\n";

            if (getDialerAndFlushCall()) {
                if (!ptr.pref.getString("skypeCheck", "0").equals("1")) {
                    ptr.saveSkypeCheck("1");
                    showAlertSkype();
                    return;
                }

            }
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + "+390810343062"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);

            final Handler resume = new Handler();
            resume.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(ptr, ptr.getClass());
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    ptr.startActivity(intent);
                    resume.removeCallbacksAndMessages(null);
                }
            }, 1000);

        } else {

            if (isAppending) {
                Log.d("HAX","recorsive");
                final Handler resume = new Handler();
                resume.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        makeCall();
                        resume.removeCallbacksAndMessages(null);
                    }
                }, 1000);

            }

        }
        //close else for skype check

    }



    private static void speedDownloadFiles(String uri, int ammo) {

        dm = (DownloadManager) ptr.getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(uri));
        Log.d("vitodownload","siamo prima il set");
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(true);
        request.setTitle("Throughput");
        request.setDescription("Throughput file download");
        request.setVisibleInDownloadsUi(false);

        //  request.setDestinationInExternalPublicDir(TPUT_DOWNLOAD_PATH, "tput.file");
        Log.d("vitodownload","siamo dopo il set");
        for(int i = 0; i < ammo; i++) {
            lastIdRef= dm.enqueue(request);
            indexRef++;
            Log.d("morpheus",lastIdRef+"");
        }



    }


    public static void removeDownloadZombies() {

        //worst case, 4 files.. P_P

        /* do not try to call all queue, this will lead to I/O issue in Nougat */
        for(int i=0; i<indexRef; i++) {
            dm.remove(lastIdRef-i);
        }

        if(onComplete!=null && dlqueueregistered == true) {
            ptr.unregisterReceiver(onComplete);
            dlqueueregistered = false;
        }

        Log.d("goes",lastIdRef+"");

    }

    public static long returnCurrentRxKBytes(int uid) {
        return Math.round(getUidRxBytes(uid) / 1024);
    }

    protected static void startUpload() {
        //goes bad!
        counterListUP = 0;
        speedListUP = new long[50];

        if(speedTestSocket!=null) speedTestSocket.closeSocket();
        speedTestSocket = new SpeedTestSocket();
        speedTestSocket.setComputationMethod(ComputationMethod.MEDIAN_INTERVAL);
        speedTestSocket.setSocketTimeout(20000);

        validateUpload = true;
        startSpeedUpload();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopUpload();
            }
        }, 15 * 1000);
    }

    protected static void stopUpload() {

        speedTestSocket.closeSocket();

        validateUpload = false;

        long speedTotUP = 0;
        for(int i =0; i<counterListUP; i++){
            speedTotUP = speedTotUP + speedListUP[i];
        }


        long speedAverageUp = 0;
        if(counterListUP!=0) speedAverageUp = speedTotUP/counterListUP;
        else speedAverageUp = 0;

        speedSTup =  speedAverageUp;
        lastULavr = speedSTup;

        for(int i=0; i <counterListUP; i++)
            Log.d("upload",speedListUP[i]+"");

        Log.d("upload",counterListUP+"");

        stopAppending();
    }


    protected static void startDownload() {

        dm = (DownloadManager) mContext.getSystemService(DOWNLOAD_SERVICE);

        if(ptr!=null) {
            uid = ptr.getApplication().getApplicationInfo().uid;
            dlqueueregistered = true;
            ptr.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        }

        //reset vars pass
        startReceivedKBytes = returnCurrentRxKBytes(uid);
        currentReceivedKBytes = 0;
        totalReceivedKBytes = 0;

        //First round..10 bullets
        speedDownloadFiles(MASSIVE_URL,10);

    }

    public static void resetForNewTest(){

        debugBody = "";
        body = "";
        dlcounter = 0;
        dlAvr=0;
        dlSum = 0;
        iterationForST = 1;
        instaspeed = 0;
        instaPing = 0;
        validateUpload=false;
        shouldAsk = true;
        stateForST = 0;
        dropped = 0;
        dlqueueregistered = false;
        idleSum= 0;
        sumPing = 0;
        counterListPing = 0;
        counterIdle= 0;
        maxSpeedDL = 0;
        maxSpeedUL = 0;
        speedSTdl=0;
        speedSTup=0;
        refSpeed=0;
        pingST = "failed";

    }


    private static boolean deleteAllFiles() {
        Log.d(TAG, "siamo dentro il delete");
        File folder = Environment.getExternalStoragePublicDirectory(TPUT_DOWNLOAD_PATH);
        // String TPUT_down = "/storage/emulated/0/tput_downloads/";

       // File folder = new File ("/storage/emulated/0/tput_downloads/");
        Log.d(TAG, "Deleting: " + folder.getPath());

        boolean result = false;

        /*if (folder.isDirectory()) {
          Log.d("Deleting", "folder Ã¨ una directory");
           Log.d("Deleting", "folder list Ã¨ " + folder.list());


          String[] children = folder.list();
            for (int i = 0; i < children.length; i++) {
                File child = new File(folder, children[i]);
                result = child.delete();
            }
        }*/

        return true;
    }



    static BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {

            // your code
            if (isAppending) {

                if(dm.STATUS_RUNNING == 2){
                    speedDownloadFiles(MASSIVE_URL, 1);
                }

                else {
                    speedDownloadFiles(MASSIVE_URL, 10);
                }

                bufferForDelete++;
            }

            if (bufferForDelete > 10) {
                bufferForDelete = 0;
                deleteAllFiles();
            }
        }
    };

    public static void monitorSpeedTestDownload(final int uid) {

        currentReceivedKBytes = returnCurrentRxKBytes(uid);
        totalReceivedKBytes = currentReceivedKBytes - startReceivedKBytes;

        //Speed
        long speed = (long) (totalReceivedKBytes / (APPEND_TIME / (double) 1024)) * 8;

        if(maxSpeedDL<speed){
            maxSpeedDL = speed;
        }

        if(testType==4) {
            instaspeed = speed;
            monitorSpeedDown(speed);
        }

        else if(testType==2){
            instaspeed = speed;
            dlcounter++;
            dlSum += speed;
        }

        //Speed report
        Log.d("DOWNLOAD TEST",speed+"");
        if(ptr!=null) {
            ptr.speedView.speedTo(speed / 1000);
        }

        startReceivedKBytes = returnCurrentRxKBytes(uid);

    }


    //SEMAPHORE (not real one) FOR SPEED TEST
    public static void semaphoreForST(){

        if(stateForST==0){

            cloneIteration = iterationForST;
            iAmSpeedTest = true;

            retryAttempsPing = 0;
            retryAttemps = 0;
            shouldAsk = true;

            pingAverage = "";
            counterListUP = 0;
            counterListDL = 0;
            counterListPing = 0;
            speedListDL = new long[50];
            speedListUP = new long[50];
            speedListPing = new float[50];
            logSpeedTest = new StringBuffer();

            if(speedTestSocket!=null) speedTestSocket.closeSocket();
            speedTestSocket = new SpeedTestSocket();
            speedTestSocket.setComputationMethod(ComputationMethod.MEDIAN_INTERVAL);


            stateForST = 1;
            semaphorePhase1();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    stateForST = 2;
                    semaphorePhaseDelay();
                    //download duration
                }
            }, 12 * 1000);

            return;
        }

        if(stateForST==2){


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    stateForST = 3;
                    semaphorePhase2();
                }
            },  200); //was 16s
            return;
        }


        if(stateForST==3){

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    stateForST = 4;
                    validateUpload = true;
                    semaphorePhaseDelayTwo();
                    //but now, this is independent from time
                }
            }, 8 * 1000);
            return;
        }


        if(stateForST==4){

            recon = new Handler();
            ret = new Runnable() {
                @Override
                public void run() {
                    stateForST = 5;
                    //check if upload is ok before ping
                    shouldAsk = false;
                    checkValideUploadround();
                }
            };

            recon.postDelayed(ret, 10 * 1000);
            return;
        }



        if(stateForST==5){

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    stateForST = 6;
                    //check if upload is ok before ping
                    checkValidePinground();
                }
            }, 8 * 1000);
            return;
        }


        //check for next test iteration
        if(stateForST == 6) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(iterationForST > 1) {
                        nextIteration();
                        validateUpload = false;
                    }
                    else {
                        stateForST = -1;
                        iterationForST = 0;
                        semaphorePhaseEnd();
                        validateUpload = false;
                    }
                }
            }, 2000);
            return;
        }


    }



    public static void nextIteration() {

        if(speedTestSocket!=null) speedTestSocket.closeSocket();
        speedTestSocket = new SpeedTestSocket();

        retryAttempsPing = 0;
        retryAttemps = 0;
        shouldAsk = true;

        iterationForST--;
        stateForST = 1;
        semaphorePhase1();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stateForST = 2;
                semaphorePhaseDelay();
            }
        }, 16 * 1000);
    }



    public static void semaphorePhase1() {
        ptr.displayState.setText("Speedtest : Download");
        startDownload();
    }

    public static void semaphorePhase2() {
        semaphoreForST();
    }


    public static void pingNoHipCup() {


        new Thread(new Runnable() {
            public void run(){

                String pingRes = getPingResult("5.249.135.85");
                Log.d("kol",pingRes+"");

                // appendSpeedTestData(pingRes);

                if(pingRes!=null) {
                    //no more NewLine mark
                    pingRes = pingRes.substring(0, pingRes.length() - 1);
                    pingRes = pingRes.substring(pingRes.lastIndexOf("\n"));
                    pingRes = pingRes.substring(pingRes.indexOf("/", 16));
                    pingRes = pingRes.substring(1, 8);

                    String bufferChar;
                    bufferChar = String.valueOf(pingRes.charAt(pingRes.length() - 1));

                    if (!bufferChar.equals("1") &&
                            !bufferChar.equals("2") &&
                            !bufferChar.equals("3") && !bufferChar.equals("4") &&
                            !bufferChar.equals("5") &&
                            !bufferChar.equals("6") &&
                            !bufferChar.equals("7") &&
                            !bufferChar.equals("8") &&
                            !bufferChar.equals("9") &&
                            !bufferChar.equals("0") &&
                            !bufferChar.equals(".")
                    ) {
                        pingRes = pingRes.substring(0, pingRes.length() - 1);
                    }


                    speedListPing[counterListPing] = Float.valueOf(pingRes);
                    counterListPing++;
                    retryAttempsPing = 0;
                }

                else{

                    if(retryAttempsPing<5){
                        retryAttempsPing++;
                        Log.d("ahi","ping for :"+retryAttempsPing);
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        run();
                        return;
                    }

                    speedListPing[counterListPing] = 0;
                    counterListPing++;
                    retryAttempsPing = 0;
                }

            }
        }).start();


    }


    public static void pingLooper() {


        new Thread(new Runnable() {
            public void run(){

                String pingRes = getPingResult("5.249.135.85");
                Log.d("kol",pingRes+"");

                if(pingRes!=null) {
                    //no more NewLine mark
                    pingRes = pingRes.substring(0, pingRes.length() - 1);
                    pingRes = pingRes.substring(pingRes.lastIndexOf("\n"));
                    pingRes = pingRes.substring(pingRes.indexOf("/", 16));
                    pingRes = pingRes.substring(1, 8);

                    String bufferChar;
                    bufferChar = String.valueOf(pingRes.charAt(pingRes.length() - 1));

                    if (!bufferChar.equals("1") &&
                            !bufferChar.equals("2") &&
                            !bufferChar.equals("3") && !bufferChar.equals("4") &&
                            !bufferChar.equals("5") &&
                            !bufferChar.equals("6") &&
                            !bufferChar.equals("7") &&
                            !bufferChar.equals("8") &&
                            !bufferChar.equals("9") &&
                            !bufferChar.equals("0") &&
                            !bufferChar.equals(".")
                    ) {
                        pingRes = pingRes.substring(0, pingRes.length() - 1);
                    }


                    instaPing = Float.valueOf(pingRes);
                }


            }
        }).start();


    }


    public static void semaphorePhase3() {

        validateUpload = false;

        ptr.displayState.setText("Speedtest : Ping");
        semaphoreForST();
        pingNoHipCup();

    }

    public static void semaphorePhase4() {
        semaphoreForST();
        ptr.displayState.setText("Check for next iteration..");
    }


    public static void semaphoreAbort() {
        ptr.displayState.setText("No Response from server!");
        iAmSpeedTest = false;
        isAppending = false;
        stopGps();
        ptr.runOnUiThread(ptr.errorMex);

        long speedTotUP = 0;
        for(int i =0; i<counterListUP; i++){
            speedTotUP = speedTotUP + speedListUP[i];
        }

        long speedTotDL = 0;
        for(int i =0; i<counterListDL; i++){
            speedTotDL = speedTotDL + speedListDL[i];
        }

        float pingTot = 0;
        for(int i =0; i<counterListPing; i++){
            pingTot = pingTot + speedListPing[i];
        }


        long speedAverageUp;
        if(counterListUP!=0) speedAverageUp = speedTotUP/counterListUP; else speedAverageUp = 0;

        long speedAverageDl;
        if(counterListDL!=0) speedAverageDl = speedTotDL/counterListDL; else speedAverageDl = 0;

        float timeAveragePing;
        if(counterListPing!=0) timeAveragePing = pingTot/counterListPing; else timeAveragePing = 0;

        speedSTdl = speedAverageDl;
        speedSTup =  speedAverageUp;

        Log.d("meeeed", speedAverageUp + " " + speedAverageDl);

        lastDLavr = speedSTdl;
        lastULavr = speedSTup;

        if(timeAveragePing!=0) pingST = Float.toString(timeAveragePing);
        else pingST = "failed";

    }

    public static void semaphorePhaseDelay() {

        removeDownloadZombies();
        bufferForDelete = 0;
        deleteAllFiles();
        ptr.displayState.setText("Speedtest : Preparing Upload");

        //goes bad!
        startSpeedUpload();

        semaphoreForST();

    }



    public static void semaphorePhaseDelayTwo(){

        //start to check if we're good.
        goForCheck();

        ptr.displayState.setText("Speedtest : Upload");
        semaphoreForST();
    }




    //Mock file sender.
    public static void semaphorePhaseEnd() {

        long speedTotUP = 0;
        for(int i =0; i<counterListUP; i++){
            speedTotUP = speedTotUP + speedListUP[i];
        }

        long speedTotDL = 0;
        for(int i =0; i<counterListDL; i++){
            speedTotDL = speedTotDL + speedListDL[i];
        }

        float pingTot = 0;
        for(int i =0; i<counterListPing; i++){
            pingTot = pingTot + speedListPing[i];
        }




        long speedAverageUp;
        if(counterListUP!=0) speedAverageUp = speedTotUP/counterListUP; else speedAverageUp = 0;

        long speedAverageDl;
        if(counterListDL!=0) speedAverageDl = speedTotDL/counterListDL; else speedAverageDl = 0;

        float timeAveragePing;
        if(counterListPing!=0) timeAveragePing = pingTot/counterListPing; else timeAveragePing = 0;

        speedSTdl = speedAverageDl;
        speedSTup =  speedAverageUp;

        Log.d("meeeed", speedAverageUp + " " + speedAverageDl);

        lastDLavr = speedSTdl;
        lastULavr = speedSTup;

        if(timeAveragePing!=0) {
            pingST = Float.toString(timeAveragePing);
            lastPing = pingST;
        }
        else {
            pingST = "failed";
            lastPing = pingST;
        }

        //blockSpeedTest();
        stopAppending();

    }



    public static void goForCheck() {

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                if (shouldAsk) {

                    if (retryAttemps < 5) {
                        retryAttemps++;
                        Log.d("again","retryAttemp : "+retryAttemps);
                        speedTestSocket.forceStopTask();
                        startSpeedUpload();
                        handler.postDelayed(this, 3500);
                    }



                    recon.removeCallbacks(ret);
                    ret = new Runnable() {
                        @Override
                        public void run() {
                            stateForST = 5;
                            //check if upload is ok before ping
                            shouldAsk = false;
                            checkValideUploadround();
                        }
                    };
                    recon.postDelayed(ret,13000);
                }


            }


        };

        handler.postDelayed(runnable, 3500);


    }


    public static void startSpeedUpload() {
        SpeedTestTask speedTestDown = new SpeedTestTask();
        speedTestDown.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "upload");
    }

    public static String getPingResult(String a) {
        String str = "";
        String result = "";
        BufferedReader reader = null;
        char[] buffer = new char[4096];
        StringBuffer output = new StringBuffer();

        try {
            Runtime r = Runtime.getRuntime();
            Process process = r.exec("/system/bin/ping -c 4 -i 100  " + a);
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            int i;

            while ((i = reader.read(buffer)) > 0)
                output.append(buffer, 0, i);


            str = output.toString();

            final String[] b = str.split("---");
            final String[] c = b[2].split("rtt");

            if (b.length == 0 || c.length == 0)
                return null;

            if(b.length == 1 || c.length == 1)
                return null;

            result = b[1].substring(1, b[1].length()) + c[0] + c[1].substring(1, c[1].length());

        } catch (IOException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
        finally
        {
            if(reader != null)
            {
                try{reader.close();}catch(IOException ie){}
            }
        }

        return result;
    }


    public static void checkValideUploadround() {
        Log.d("again","i have been called");
        //TODO : check validation
        retryAttemps = 0;

        long speedTotDL = 0;
        for(int i =0; i<counterListDL; i++){
            speedTotDL = speedTotDL + speedListDL[i];
            Log.d("wtf","help dl:"+speedTotDL+" : "+i);
        }

        long speedTotUP = 0;
        for(int i =0; i<counterListUP; i++){
            speedTotUP = speedTotUP + speedListUP[i];
            Log.d("wtf","help up:" +speedTotUP+" : "+i);
        }

        if(speedTotUP!=0 && speedTotDL!=0) semaphorePhase3();
        else {
            Log.d("wtf","help : UP "+speedTotUP+" and DL : "+speedTotDL);
            stateForST = -1;
            iterationForST = 0;
            validateUpload = false;
            semaphoreAbort();
        }
    }




    public static void checkValidePinground() {
        //TODO : check validation
        float speedTotPing = 0;
        for(int i =0; i<counterListPing; i++){
            speedTotPing = speedTotPing + speedListPing[i];
        }

        semaphorePhase4();
    }


    public static void monitorSpeedDown(long speed){
        speedListDL[counterListDL] = speed;
        counterListDL++;
        Log.d("stampare",counterListDL+ " : counterListDL");
        Log.d("stampare",cloneIteration+ " : cloneIterator");
        //appendSpeedTestData(getNeighborgNetworkInfosForLog(mTelephonyManager, latSpeed, lonSpeed ,String.valueOf(speed), "TPT_LOG", false)+"DOWNLOAD");
    }

    public static void monitorSpeedUp(long speed){

        if(maxSpeedUL<speed){
            maxSpeedUL = speed;
        }

        instaspeed = speed;

        if(ptr!=null) {
            //ptr.speedView.setSpeedAt(speed);
            refSpeed = speed/1000;
        }
    }



    public static class SpeedTestTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            speedTestSocket = new SpeedTestSocket();

            if(params[0] == "upload2") {
                speedTestSocket.addSpeedTestListener(new ISpeedTestListener() {

                    @Override
                    public void onCompletion(SpeedTestReport report) {
                        // called when download/upload is complete
                        //  masterInstance.gaugeLimitValue = 0;
                    }

                    @Override
                    public void onError(SpeedTestError speedTestError, String errorMessage) {
                        // called when a download/upload error occur
                    }

                    @Override
                    public void onProgress(float percent, SpeedTestReport report) {
                        BigDecimal a = speedTestSocket.getLiveReport().getTransferRateBit();//report.getTransferRateBit();
                        BigDecimal b = new BigDecimal("1000");
                        BigDecimal c = a.divide(b,4, BigDecimal.ROUND_HALF_UP);
                        Log.d("kol",c+" UP");
                        // masterInstance.gaugeLimitValue = c.longValue();

                        monitorSpeedUp(c.longValue());

                    }
                });

                String fileName = SpeedTestUtils.generateFileName() + ".txt";
                speedTestSocket.startFixedUpload("ftp://tconftp:Tc0Nn3c72oo18!@5.249.135.85/upload/"+fileName, 30000000, 10000,1000);
            }

            if(params[0] == "upload") {
                Log.d("kol","upload phase");
                String fileName = SpeedTestUtils.generateFileName() + ".txt";

                speedTestSocket.startUploadRepeat("ftp://tconftp:Tc0Nn3c72oo18!@5.249.135.85/upload/"+fileName, 20000,
                        3000, 20000000, new
                                IRepeatListener() {
                                    @Override
                                    public void onCompletion(final SpeedTestReport report) {
                                        // masterInstance.gaugeLimitValue = 0;
                                        // goOnFromHere = true;
                                    }

                                    @Override
                                    public void onReport(final SpeedTestReport report) {
                                        BigDecimal a = report.getTransferRateBit();
                                        BigDecimal b = new BigDecimal("1000");
                                        BigDecimal c = a.divide(b,4, BigDecimal.ROUND_HALF_UP);
                                        Log.d("kol",c+" UP");

                                        if(validateUpload) {
                                            shouldAsk = false;
                                            //masterInstance.gaugeLimitValue = c.longValue();
                                            monitorSpeedUp(c.longValue());
                                        }
                                    }
                                });
            }


            return null;
        }
    }




    private static int getFrequencyByRFCN(String rfcnS) {

        /* No need for tables, let's unroll. yeahhh! */

        int rfcn = Integer.valueOf(rfcnS);

        int mhz;

        switch (rfcn) {
            //LTE

            case 350 :
                mhz = 2100; //wind3
                break;

            case 2900: //iliad
                mhz = 2600;
                break;

            case 3025: //tim
                mhz = 2600;
                break;

            case 3175: //voda
                mhz = 2600;
                break;

            case 3350: //wind3
                mhz = 2600;
                break;

            case 3356: //wind3
                mhz = 2600;
                break;

            case 1350://tim
                mhz = 1800;
                break;

            case 1850: //voda
                mhz = 1800;
                break;

            case 1325: //wind3-iliad
                mhz = 1800;
                break;

            case 1650: //wind3
                mhz = 1800;
                break;

            case 1675: //wind3
                mhz = 1800;
                break;

            case 6200: //wind3
                mhz = 800;
                break;

            case 6300: //tim
                mhz = 800;
                break;

            case 6400: //voda
                mhz = 800;
                break;

            //WCDMA
            case 10738:
                mhz = 2100;
                break;
            case 10713:
                mhz = 2100;
                break;
            case 10763:
                mhz = 2100;
                break;
            case 10588:
                mhz = 2100;
                break;
            case 10563:
                mhz = 2100;
                break;
            case 10613:
                mhz = 2100;
                break;
            case 10638: //tim
                mhz = 2100;
                break;
            case 10663: //tim
                mhz = 2100;
                break;
            case 10688://tim
                mhz = 2100;
                break;
            case 10788://voda
                mhz = 2100;
                break;
            case 10813://voda
                mhz = 2100;
                break;
            case 10838://voda
                mhz = 2100;
                break;

            case 3038: //voda
                mhz = 900;
                break;

            case 3013: //voda
                mhz = 900;
                break;

            case 2988: //tim
                mhz = 900;
                break;

            case 2963: //tim
                mhz = 900;
                break;

            case 2938: //wind3
                mhz = 900;
                break;
            case 3063: //wind3
                mhz = 900;
                break;

            default: //GSM
                //1800..
                if (rfcn > 687 && rfcn < 711) { // compreso tra 688 e 710
                    mhz = 1800;
                }

                else if (rfcn > 536 && rfcn < 561) { //tim
                    mhz = 1800;
                }

                else if (rfcn > 561 && rfcn < 586) { //tim
                    mhz = 1800;
                }

                else if (rfcn > 586 && rfcn < 611) { //tim
                    mhz = 1800;
                }

                else if (rfcn > 611 && rfcn < 636) { //tim
                    mhz = 1800;
                }

                else if (rfcn > 786 && rfcn < 811) { //voda
                    mhz = 1800;
                }

                else if (rfcn > 811 && rfcn < 836) { //voda
                    mhz = 1800;
                }

                else if (rfcn > 836 && rfcn < 861) { //voda
                    mhz = 1800;
                }

                else if (rfcn > 861 && rfcn < 886) { //voda
                    mhz = 1800;
                }


                else if (rfcn > 100 && rfcn < 125) { // compreso tra 101 e 124
                    mhz = 900;
                }

                else if (rfcn > 999 && rfcn < 1024) { //tim
                    mhz = 900;
                }

                else if (rfcn > 0 && rfcn < 25) { //tim
                    mhz = 900;
                }

                else if (rfcn > 25 && rfcn < 50) { //voda
                    mhz = 900;
                }

                else if (rfcn > 50 && rfcn < 75) { //voda
                    mhz = 900;
                }


                else {
                    mhz = 0;
                }

                break;
        }

        return mhz;
    }



}