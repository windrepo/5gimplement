package net.wind3.youonetfork.layout;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import net.wind3.youonetfork.R;

import pl.droidsonroids.gif.GifImageView;


public class ButtonStop extends ConstraintLayout {

    LayoutInflater inflater;
    Context mContext;
    AttributeSet mAttrs;

    GifImageView bt;

    public ButtonStop(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
        mContext = context;
        mAttrs = attrs;
        inflater.inflate(R.layout.button_stop, this);

    }

    public void changeBackgroundToPress() {
        inflater.inflate(R.layout.button_stop_press,this);
    }

    public void changeBackgroundToBump() {
        inflater.inflate(R.layout.button_stop,this);
    }

    public void changeBackgroundToNC() {
        inflater.inflate(R.layout.button_stop_nc,this);
    }

    public void changeBackgroundToCompleted() {
        inflater.inflate(R.layout.button_completed,this);
    }

    public void changeBackgroundToCompletedF() {
        inflater.inflate(R.layout.button_completedfinal,this);
    }


    public void changeBackgroundToError() {
        inflater.inflate(R.layout.button_error,this);
    }

    public void changeBackgroundToAUTO() {
        inflater.inflate(R.layout.button_stop_auto,this);
    }

}