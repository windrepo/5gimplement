package net.wind3.youonetfork.layout;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import net.wind3.youonetfork.R;


public class ButtonGoBack extends ConstraintLayout {


    LayoutInflater inflater;
    Context mContext;
    AttributeSet mAttrs;


    public ButtonGoBack(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
        mContext = context;
        mAttrs = attrs;
        inflater.inflate(R.layout.button_goback, this);
    }


    public void changeBackgroundToPress()
    {
        inflater.inflate(R.layout.button_goback_press,this);
    }

    public void changeBackgroundToBump() {
        inflater.inflate(R.layout.button_goback,this);
    }

}