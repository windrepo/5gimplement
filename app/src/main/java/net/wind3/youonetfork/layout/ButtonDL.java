package net.wind3.youonetfork.layout;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import net.wind3.youonetfork.R;


public class ButtonDL extends ConstraintLayout {


    LayoutInflater inflater;
    Context mContext;
    AttributeSet mAttrs;

    public ButtonDL(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
        mContext = context;
        mAttrs = attrs;
        inflater.inflate(R.layout.button_download, this);
    }

    public void changeBackgroundToPress() {
        inflater.inflate(R.layout.button_download_press,this);
    }

    public void changeBackgroundToBump() {
        inflater.inflate(R.layout.button_download,this);
    }
}