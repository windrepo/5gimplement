package net.wind3.youonetfork.layout;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import net.wind3.youonetfork.R;


public class ButtonStart extends ConstraintLayout {
    public ButtonStart(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.button_start, this);
    }
}