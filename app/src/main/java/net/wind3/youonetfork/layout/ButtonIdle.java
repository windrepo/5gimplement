package net.wind3.youonetfork.layout;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import net.wind3.youonetfork.R;


public class ButtonIdle extends ConstraintLayout {

    LayoutInflater inflater;
    Context mContext;
    AttributeSet mAttrs;

    public ButtonIdle(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
        mContext = context;
        mAttrs = attrs;
        inflater.inflate(R.layout.button_idle, this);
    }

    public void changeBackgroundToPress() {
        inflater.inflate(R.layout.button_idle_press,this);
    }

    public void changeBackgroundToBump() {
        inflater.inflate(R.layout.button_idle,this);
    }

}