package net.wind3.youonetfork.layout;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import net.wind3.youonetfork.R;


public class ButtonPath extends ConstraintLayout {

    LayoutInflater inflater;
    Context mContext;
    AttributeSet mAttrs;

    public ButtonPath(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
        mContext = context;
        mAttrs = attrs;
        inflater.inflate(R.layout.button_path, this);
    }

    public void changeBackgroundToPress() {
        inflater.inflate(R.layout.button_path_press,this);
    }

    public void changeBackgroundToBump() {
        inflater.inflate(R.layout.button_path,this);
    }

}