package net.wind3.youonetfork;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.ubudu.gmaps.MapLayout;
import com.ubudu.gmaps.factory.MarkerOptionsFactory;
import com.ubudu.gmaps.model.Marker;
import com.ubudu.gmaps.model.Zone;
import com.ubudu.gmaps.util.MarkerOptionsStrategy;

import net.wind3.youonetfork.services.TestService;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static net.wind3.youonetfork.MainActivity.dpToPx;


public class MapActivity extends AppCompatActivity {

    public static final String CONTENT_TYPE = "application/json";

    MapLayout mMapLayout;
    Button mGrid;
    Button mFake;
    Button mCounty;
    Button mToggle;
    Button mMode;
    Button mWhere;
    ImageView mLegend;
    BottomNavigationView navigation;

    public TextView infos;
    public View hook;
    public View whiteboard;

    static MainActivity master;
    LatLng sd;
    CameraUpdate markerUpdate;

    int bufferIndex = 0;
    int sessionsIndex = 0;
    int pathIndex = 0;


    //We are using arrays and not list for performance
    double lat[];
    double lng[];
    String cell[];

    String sessions[];
    String date[];
    String time[];

    String[] helpForDisplay;

    String latPath[];
    String qualPath[];
    String speedPath[];
    String speedPathUl[];
    String typePath[];
    String longPath[];

    Marker servMark;
    Marker meMark;
    String cellServ;
    String cellInfo = "";

    int cellid = 0;
    int tac = 0;
    int pci = 0;
    int rat = 0;

    int globalLenght = 0;

    String[] provs;
    String[] provsCD;
    String[] provsJson;

    //table more details info
    String allCell[];
    String allFrequency[];
    String allPCI[];
    String allPSC[];
    String allBSIC[];
    String allRNC[];
    String allAzimuth[];
    String allCI[];
    String allLac[];
    String allEnode[];
    String allTac[];

    //Table Kill Frenzy!!

    View v;
    TableRow r1;
    boolean hackClicked = false;
    TextView s1;

    TextView m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11;
    TextView m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22;


    protected LocationManager mLocationManager;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_dev5);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Map view");
        actionBar.setDisplayHomeAsUpEnabled(true);

        //remove title bar..and init framework rendering flags
        getWindow().setStatusBarColor(Color.rgb(108, 166, 205));


        final int lFlags = getWindow().getDecorView().getSystemUiVisibility();
        getWindow().getDecorView().setSystemUiVisibility(true ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));

        getSupportActionBar().hide();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        /* Ok, this part is a mess. There were simply too many requests at the last time, we tried our best */

        provsJson = new String[24000];

        provs = new String[111];
        provs[0] = "---";
        provs[1] = "AGRIGENTO";
        provs[2] = "ALESSANDRIA";
        provs[3] = "ANCONA";
        provs[4] = "AOSTA";
        provs[5] = "AREZZO";
        provs[6] = "ASCOLI PICENO";
        provs[7] = "ASTI";
        provs[8] = "AVELLINO";
        provs[9] = "BARI";
        provs[10] = "BARLETTA";
        provs[11] = "BELLUNO";
        provs[12] = "BENEVENTO";
        provs[13] = "BERGAMO";
        provs[14] = "BIELLA";
        provs[15] = "BOLOGNA";
        provs[16] = "BOLZANO";
        provs[17] = "BRESCIA";
        provs[18] = "BRINDISI";
        provs[19] = "CAGLIARI";
        provs[20] = "CALTANISSETTA";
        provs[21] = "CAMPOBASSO";
        provs[22] = "CARBONIA-IGLESIAS";
        provs[23] = "CASERTA";
        provs[24] = "CATANIA";
        provs[25] = "CATANZARO";
        provs[26] = "CHIETI";
        provs[27] = "COMO";
        provs[28] = "COSENZA";
        provs[29] = "CREMONA";
        provs[30] = "CROTONE";
        provs[31] = "CUNEO";
        provs[32] = "ENNA";
        provs[33] = "FERMO";
        provs[34] = "FERRARA";
        provs[35] = "FIRENZE";
        provs[36] = "FOGGIA";
        provs[37] = "FORLI-CESENA";
        provs[38] = "FROSINONE";
        provs[39] = "GENOVA";
        provs[40] = "GORIZIA";
        provs[41] = "GROSSETO";
        provs[42] = "IMPERIA";
        provs[43] = "ISERNIA";
        provs[44] = "LA SPEZIA";
        provs[45] = "L'AQUILA";
        provs[46] = "LATINA";
        provs[47] = "LECCE";
        provs[48] = "LECCO";
        provs[49] = "LIVORNO";
        provs[50] = "LODI";
        provs[51] = "LUCCA";
        provs[52] = "MACERATA";
        provs[53] = "MANTOVA";
        provs[54] = "MASSA-CARRARA";
        provs[55] = "MATERA";
        provs[56] = "MESSINA";
        provs[57] = "MILANO";
        provs[58] = "MODENA";
        provs[59] = "MONZA E DELLA BRIANZA";
        provs[60] = "NAPOLI";
        provs[61] = "NOVARA";
        provs[62] = "NUORO";
        provs[63] = "OLBIA-TEMPIO";
        provs[64] = "ORISTANO";
        provs[65] = "PADOVA";
        provs[66] = "PALERMO";
        provs[67] = "PARMA";
        provs[68] = "PAVIA";
        provs[69] = "PERUGIA";
        provs[70] = "PESARO E URBINO";
        provs[71] = "PESCARA";
        provs[72] = "PIACENZA";
        provs[73] = "PISA";
        provs[74] = "PISTOIA";
        provs[75] = "PORDENONE";
        provs[76] = "POTENZA";
        provs[77] = "PRATO";
        provs[78] = "RAGUSA";
        provs[79] = "RAVENNA";
        provs[80] = "REGGIO CALABRIA";
        provs[81] = "REGGIO EMILIA";
        provs[82] = "RIETI";
        provs[83] = "RIMINI";
        provs[84] = "ROMA";
        provs[85] = "ROVIGO";
        provs[86] = "SALERNO";
        provs[87] = "MEDIO CAMPIDANO";
        provs[88] = "SASSARI";
        provs[89] = "SAVONA";
        provs[90] = "SIENA";
        provs[91] = "SIRACUSA";
        provs[92] = "SONDRIO";
        provs[93] = "TARANTO";
        provs[94] = "TERAMO";
        provs[95] = "TERNI";
        provs[96] = "TORINO";
        provs[97] = "OGLIASTRA";
        provs[98] = "TRAPANI";
        provs[99] = "TRENTO";
        provs[100] = "TREVISO";
        provs[101] = "TRIESTE";
        provs[102] = "UDINE";
        provs[103] = "VARESE";
        provs[104] = "VENEZIA";
        provs[105] = "VERBANO-CUSIO-OSSOLA";
        provs[106] = "VERCELLI";
        provs[107] = "VERONA";
        provs[108] = "VIBO VALENTIA";
        provs[109] = "VICENZA";
        provs[110] = "VITERBO";


        provsCD = new String[111];
        provsCD[0] = "---";
        provsCD[1] = "AG";
        provsCD[2] = "AL";
        provsCD[3] = "AN";
        provsCD[4] = "AO";
        provsCD[5] = "AR";
        provsCD[6] = "AP";
        provsCD[7] = "AT";
        provsCD[8] = "AV";
        provsCD[9] = "BA";
        provsCD[10] = "BT";
        provsCD[11] = "BL";
        provsCD[12] = "BN";
        provsCD[13] = "BG";
        provsCD[14] = "BI";
        provsCD[15] = "BO";
        provsCD[16] = "BZ";
        provsCD[17] = "BS";
        provsCD[18] = "BR";
        provsCD[19] = "CA";
        provsCD[20] = "CL";
        provsCD[21] = "CB";
        provsCD[22] = "CI";
        provsCD[23] = "CE";
        provsCD[24] = "CT";
        provsCD[25] = "CZ";
        provsCD[26] = "CH";
        provsCD[27] = "CO";
        provsCD[28] = "CS";
        provsCD[29] = "CR";
        provsCD[30] = "KR";
        provsCD[31] = "CN";
        provsCD[32] = "EN";
        provsCD[33] = "FM";
        provsCD[34] = "FE";
        provsCD[35] = "FI";
        provsCD[36] = "FG";
        provsCD[37] = "FC";
        provsCD[38] = "FR";
        provsCD[39] = "GE";
        provsCD[40] = "GO";
        provsCD[41] = "GR";
        provsCD[42] = "IM";
        provsCD[43] = "IS";
        provsCD[44] = "SP";
        provsCD[45] = "AQ";
        provsCD[46] = "LT";
        provsCD[47] = "LE";
        provsCD[48] = "LC";
        provsCD[49] = "LI";
        provsCD[50] = "LO";
        provsCD[51] = "LU";
        provsCD[52] = "MC";
        provsCD[53] = "MN";
        provsCD[54] = "MS";
        provsCD[55] = "MT";
        provsCD[56] = "ME";
        provsCD[57] = "MI";
        provsCD[58] = "MO";
        provsCD[59] = "MB";
        provsCD[60] = "NA";
        provsCD[61] = "NO";
        provsCD[62] = "NU";
        provsCD[63] = "OT";
        provsCD[64] = "OR";
        provsCD[65] = "PD";
        provsCD[66] = "PA";
        provsCD[67] = "PR";
        provsCD[68] = "PV";
        provsCD[69] = "PG";
        provsCD[70] = "PU";
        provsCD[71] = "PE";
        provsCD[72] = "PC";
        provsCD[73] = "PI";
        provsCD[74] = "PT";
        provsCD[75] = "PN";
        provsCD[76] = "PZ";
        provsCD[77] = "PO";
        provsCD[78] = "RG";
        provsCD[79] = "RA";
        provsCD[80] = "RC";
        provsCD[81] = "RE";
        provsCD[82] = "RI";
        provsCD[83] = "RN";
        provsCD[84] = "RM";
        provsCD[85] = "RO";
        provsCD[86] = "SA";
        provsCD[87] = "VS";
        provsCD[88] = "SS";
        provsCD[89] = "SV";
        provsCD[90] = "SI";
        provsCD[91] = "SR";
        provsCD[92] = "SO";
        provsCD[93] = "TA";
        provsCD[94] = "TE";
        provsCD[95] = "TR";
        provsCD[96] = "TO";
        provsCD[97] = "OG";
        provsCD[98] = "TP";
        provsCD[99] = "TN";
        provsCD[100] = "TV";
        provsCD[101] = "TS";
        provsCD[102] = "UD";
        provsCD[103] = "VA";
        provsCD[104] = "VE";
        provsCD[105] = "VB";
        provsCD[106] = "VC";
        provsCD[107] = "VR";
        provsCD[108] = "VV";
        provsCD[109] = "VI";
        provsCD[110] = "VT";

        mMapLayout = findViewById(R.id.map);
        mMapLayout.init(getApplicationContext());

        infos = findViewById(R.id.info);
        hook = findViewById(R.id.hookView);
        whiteboard = findViewById(R.id.whiteBoard);

        mGrid = findViewById(R.id.button3);
        mGrid.setBackground(master.getDrawable(R.drawable.testsuite));

        mFake = findViewById(R.id.fake_path);
        mFake.setBackground(master.getDrawable(R.drawable.path
        ));

        mCounty = findViewById(R.id.countybutton);
        mCounty.setBackground(master.getDrawable(R.drawable.testsuite));

        mToggle = findViewById(R.id.archdown);
        mToggle.setBackground(master.getDrawable(R.drawable.testsuitemap));

        mMode = findViewById(R.id.modeDetails);
        mMode.setBackground(master.getDrawable(R.drawable.testsuite));

        mWhere = findViewById(R.id.whereWe);
        mWhere.setBackground(master.getDrawable(R.drawable.center));

        navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.action_item2);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.action_item1:
                        master.navigation.setSelectedItemId(R.id.action_item1);
                        final Handler handler2 = new Handler();

                        Runnable runnable2 = new Runnable() {
                            @Override
                            public void run() {
                               finish();
                                handler2.removeCallbacksAndMessages(null);
                            }
                        };

                        handler2.postDelayed(runnable2,300);
                        break;

                    case R.id.action_item3 :
                       exitAlert();
                }
                return true;
            }
        });



        mWhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //myLocationUpdate();
                if(sd!=null) {
                    zoomRedoffsetUpdate();
                    startTransitionDown();
                }
            }


        });

        mMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              showModeDetails();
            }


        });

        mLegend = findViewById(R.id.legend);


        lat = new double[24000];
        lng = new double[24000];
        cell = new String[24000];
        sessions = new String[10000];
        date = new String[10000];
        time = new String[10000];


        latPath = new String[10000];
        longPath = new String[10000];
        qualPath = new String[10000];
        typePath = new String[10000];
        speedPath = new String[10000];
        speedPathUl = new String[10000];

        getRadios();
        getSessions();

        //init for Table

        allCell = new String[50];
        allFrequency = new String[50];
        allPCI = new String[50];
        allPSC= new String[50];
        allBSIC= new String[50];
        allRNC= new String[50];
        allAzimuth = new String[50];
        allCI= new String[50];
        allLac= new String[50];
        allTac= new String[50];
        allEnode = new String[50];

        setAllViews();

        mGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //warning, if the gps hasn't fixed yet, we must be careful with markers!
                if(mMapLayout!=null && mMapLayout.getMap()!=null && sd!=null) {
                    eraseMarker();
                    redoffsetUpdate();
                    startTransitionDown();
                    fillServ(cellServ);
                    fillMe(sd);
                 }
                }
        });


        mCounty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showPickCounty();
                if(sd!=null) {
                    offsetUpdate();
                    pickAfterUpdate();
                    startTransitionUp();
                }
            }


            });


        mToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTransitionDown();
            }


        });


        mFake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPickSession();
            }


        });



        //listener for maps
        mMapLayout.setEventListener(new MapLayout.EventListener() {

            @Override
            public void onMapReady() {
                // called when map layout is ready to handle API calls
            }

            @Override
            public void onZoneClicked(Zone zone, Polygon polygon) {
                Log.i("LOGIC","Polygon clicked: "+zone.getName()+", polygon id: "+polygon.getId());
            }

            @Override
            public void onMarkerClicked(Marker marker, com.google.android.gms.maps.model.Marker marker1) {

                String tag = marker.getTitle();

                //discard test

                mLegend.setImageDrawable(getResources().getDrawable(R.drawable.legend));

                if(tag != null && tag.contains("Dropped Call")){
                    mLegend.setImageDrawable(getResources().getDrawable(R.drawable.legend2));
                }

                if(tag != null && tag.contains("Level : ")){
                    mLegend.setImageDrawable(getResources().getDrawable(R.drawable.legend2));
                }

                if(tag != null && (tag.contains("Data Test Upload :") || tag.contains("Data Test Download :")
                        || tag.contains("SpeedTest Download : ") || tag.contains("SpeedTest Upload : ")
                        || tag.contains("SpeedTest DL + UL : "))){
                    mLegend.setImageDrawable(getResources().getDrawable(R.drawable.legend3));
                }

                if (tag != null && !tag.contains("Ping : ") && !tag.contains("Dropped Call") && !tag.contains("SpeedTest DL + UL : ")
                        && !tag.equals("You are here") && !tag.contains("Level : ")
                        && !tag.contains("SpeedTest Download : ") && !tag.contains("SpeedTest Upload : ")
                            && !tag.contains("Data Test Download :") && !tag.contains("Data Test Upload :")
                                 && infos.getText()!=null && infos.getText()!="---------") {
                    //offsetForMarker(cm);
                    startTransitionUp();
                    requestInfo(marker);
                }

                LatLng nh = new LatLng(marker1.getPosition().latitude - 0.001f, marker1.getPosition().longitude);
                CameraUpdate markerCamera = CameraUpdateFactory.newLatLngZoom(nh, 18);

                CameraPosition cm = new CameraPosition(nh, 18, 38, 2);

                markerCamera = CameraUpdateFactory.newCameraPosition(cm);

                mMapLayout.getMap().animateCamera(markerCamera);

                offsetForMarker(markerCamera);
            }



        });


    }


    public static void setMasterPtr(MainActivity ptr) {
        master = ptr;
        //@_@ ok?
    }

    public void setAllViews() {

        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null) {
                parent.removeView(v);
            }
        }
        try {
            v = getLayoutInflater().inflate(R.layout.mode_det,null);
        } catch (InflateException e) {

        }

        s1 = v.findViewById(R.id.s1);
        m1 = v.findViewById(R.id.m1);

        m2 = v.findViewById(R.id.m2);
        m3 = v.findViewById(R.id.m3);
        m4 = v.findViewById(R.id.m4);
        m5 = v.findViewById(R.id.m5);
        m6 = v.findViewById(R.id.m6);
        m7 = v.findViewById(R.id.m7);
        m8 = v.findViewById(R.id.m8);
        m9 = v.findViewById(R.id.m9);
        m10 = v.findViewById(R.id.m10);
        m11 = v.findViewById(R.id.m11);

        m12 = v.findViewById(R.id.m12);
        m13 = v.findViewById(R.id.m13);
        m14 = v.findViewById(R.id.m14);
        m15 = v.findViewById(R.id.m15);
        m16 = v.findViewById(R.id.m16);
        m17 = v.findViewById(R.id.m17);
        m18 = v.findViewById(R.id.m18);
        m19 = v.findViewById(R.id.m19);
        m20 = v.findViewById(R.id.m20);


    }


    public void myLocationUpdate(){
        if(meMark!=null) {
            LatLng offCoords = new LatLng(meMark.getLocation().latitude - 0.5f, meMark.getLocation().latitude);
            CameraUpdate offCamera = CameraUpdateFactory.newLatLngZoom(offCoords, 9);
            mMapLayout.getMap().animateCamera(offCamera);
        }
    }


    public void offsetUpdate(){
        LatLng offCoords = new LatLng(sd.latitude-0.5f,sd.longitude);
        CameraUpdate offCamera = CameraUpdateFactory.newLatLngZoom(offCoords, 9);
        mMapLayout.getMap().animateCamera(offCamera);
    }

    public void redoffsetUpdate(){
        CameraUpdate offCamera = CameraUpdateFactory.newLatLngZoom(sd, 10);
        mMapLayout.getMap().animateCamera(offCamera);
    }

    public void zoomRedoffsetUpdate(){
        CameraUpdate offCamera = CameraUpdateFactory.newLatLngZoom(sd, 15);
        mMapLayout.getMap().animateCamera(offCamera);
    }

    public void pickAfterUpdate() {
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
             @Override
             public void run() {
        runOnUiThread(picker);
            }
         }, 700);
    }



    public void exitAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage("Do you want to exit and close application? Be careful to NOT EXIT white undertest! (ATTENTION : it will NOT be running in background)")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                master.kill(); finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        navigation.setSelectedItemId(R.id.action_item2);
                        dialog.cancel();
                    }
                });
        alertDialogBuilder.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    public void offsetForMarker(CameraUpdate nh) {

        //CameraUpdate
        markerUpdate = nh;

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(clickmarker);
            }
        }, 100);
    }


    public void requestInfo(Marker marker) {
        String cellName = marker.getTitle();
        getInfos(cellName);

    }


    public void showModeDetails() {

        //TODO : refactor this..bloody mess
        //even a listview will suffice
            final Dialog d = new Dialog(this);
            d.setTitle("Mode Details");

       setAllViews();

        s1.setText("Cell"+eatSpace(allCell[0],1)+"       Band"+eatSpace(allFrequency[0],1)+"PCI"+eatSpace(allPCI[0],1)+"PSC"+eatSpace(allPSC[0],1)+"BSIC"+eatSpace(allBSIC[0],1)+"RNC"+eatSpace(allRNC[0],1)+"EnodeB"+eatSpace(allEnode[0],1)+"Azm"+eatSpace(allAzimuth[0],1)+"CI           "+eatSpace(allCI[0],1)+"Lac"+eatSpace(allLac[0],1)+"Tac" );
        m1.setText(allCell[0]+eatSpace(allCell[0],1)+allFrequency[0]+eatSpace(allFrequency[0],1)+allPCI[0]+eatSpace(allPCI[0],2)+allPSC[0]+eatSpace(allPSC[0],3)+allBSIC[0]+eatSpace(allBSIC[0],4)+allRNC[0]+eatSpace(allRNC[0],1)+allEnode[0]+eatSpace(allEnode[0],5)+allAzimuth[0]+eatSpace(allAzimuth[0],6)+allCI[0]+eatSpace(allCI[0],7)+allLac[0]+"                  "+allTac[0]);
        m2.setText(allCell[1]+eatSpace(allCell[1],1)+allFrequency[1]+eatSpace(allFrequency[1],1)+allPCI[1]+eatSpace(allPCI[1],2)+allPSC[1]+eatSpace(allPSC[1],3)+allBSIC[1]+eatSpace(allBSIC[1],4)+allRNC[1]+eatSpace(allRNC[1],1)+allEnode[1]+eatSpace(allEnode[1],5)+allAzimuth[1]+eatSpace(allAzimuth[1],6)+allCI[1]+eatSpace(allCI[1],7)+allLac[1]+"                  "+allTac[1]);
        m3.setText(allCell[2]+eatSpace(allCell[2],1)+allFrequency[2]+eatSpace(allFrequency[2],1)+allPCI[2]+eatSpace(allPCI[2],2)+allPSC[2]+eatSpace(allPSC[2],3)+allBSIC[2]+eatSpace(allBSIC[2],4)+allRNC[2]+eatSpace(allRNC[2],1)+allEnode[2]+eatSpace(allEnode[2],5)+allAzimuth[2]+eatSpace(allAzimuth[2],6)+allCI[2]+eatSpace(allCI[2],7)+allLac[2]+"                  "+allTac[2]);
        m4.setText(allCell[3]+eatSpace(allCell[3],1)+allFrequency[3]+eatSpace(allFrequency[3],1)+allPCI[3]+eatSpace(allPCI[3],2)+allPSC[3]+eatSpace(allPSC[3],3)+allBSIC[3]+eatSpace(allBSIC[3],4)+allRNC[3]+eatSpace(allRNC[3],1)+allEnode[3]+eatSpace(allEnode[3],5)+allAzimuth[3]+eatSpace(allAzimuth[3],6)+allCI[3]+eatSpace(allCI[3],7)+allLac[3]+"                  "+allTac[3]);
        m5.setText(allCell[4]+eatSpace(allCell[4],1)+allFrequency[4]+eatSpace(allFrequency[4],1)+allPCI[4]+eatSpace(allPCI[4],2)+allPSC[4]+eatSpace(allPSC[4],3)+allBSIC[4]+eatSpace(allBSIC[4],4)+allRNC[4]+eatSpace(allRNC[4],1)+allEnode[4]+eatSpace(allEnode[4],5)+allAzimuth[4]+eatSpace(allAzimuth[4],6)+allCI[4]+eatSpace(allCI[4],7)+allLac[4]+"                  "+allTac[4]);
        m6.setText(allCell[5]+eatSpace(allCell[5],1)+allFrequency[5]+eatSpace(allFrequency[5],1)+allPCI[5]+eatSpace(allPCI[5],2)+allPSC[5]+eatSpace(allPSC[5],3)+allBSIC[5]+eatSpace(allBSIC[5],4)+allRNC[5]+eatSpace(allRNC[5],1)+allEnode[5]+eatSpace(allEnode[5],5)+allAzimuth[5]+eatSpace(allAzimuth[5],6)+allCI[5]+eatSpace(allCI[5],7)+allLac[5]+"                  "+allTac[5]);
        m7.setText(allCell[6]+eatSpace(allCell[6],1)+allFrequency[6]+eatSpace(allFrequency[6],1)+allPCI[6]+eatSpace(allPCI[6],2)+allPSC[6]+eatSpace(allPSC[6],3)+allBSIC[6]+eatSpace(allBSIC[6],4)+allRNC[6]+eatSpace(allRNC[6],1)+allEnode[6]+eatSpace(allEnode[6],5)+allAzimuth[6]+eatSpace(allAzimuth[6],6)+allCI[6]+eatSpace(allCI[6],7)+allLac[6]+"                  "+allTac[6]);
        m8.setText(allCell[7]+eatSpace(allCell[7],1)+allFrequency[7]+eatSpace(allFrequency[7],1)+allPCI[7]+eatSpace(allPCI[7],2)+allPSC[7]+eatSpace(allPSC[7],3)+allBSIC[7]+eatSpace(allBSIC[7],4)+allRNC[7]+eatSpace(allRNC[7],1)+allEnode[7]+eatSpace(allEnode[7],5)+allAzimuth[7]+eatSpace(allAzimuth[7],6)+allCI[7]+eatSpace(allCI[7],7)+allLac[7]+"                  "+allTac[7]);
        m9.setText(allCell[8]+eatSpace(allCell[8],1)+allFrequency[8]+eatSpace(allFrequency[8],1)+allPCI[8]+eatSpace(allPCI[8],2)+allPSC[8]+eatSpace(allPSC[8],3)+allBSIC[8]+eatSpace(allBSIC[8],4)+allRNC[8]+eatSpace(allRNC[8],1)+allEnode[8]+eatSpace(allEnode[8],5)+allAzimuth[8]+eatSpace(allAzimuth[8],6)+allCI[8]+eatSpace(allCI[8],7)+allLac[8]+"                  "+allTac[8]);
        m10.setText(allCell[9]+eatSpace(allCell[9],1)+allFrequency[9]+eatSpace(allFrequency[9],1)+allPCI[9]+eatSpace(allPCI[9],2)+allPSC[9]+eatSpace(allPSC[9],3)+allBSIC[9]+eatSpace(allBSIC[9],4)+allRNC[9]+eatSpace(allRNC[9],1)+allEnode[9]+eatSpace(allEnode[9],5)+allAzimuth[9]+eatSpace(allAzimuth[9],6)+allCI[9]+eatSpace(allCI[9],7)+allLac[9]+"                  "+allTac[9]);
        m11.setText(allCell[10]+eatSpace(allCell[10],1)+allFrequency[10]+eatSpace(allFrequency[10],1)+allPCI[10]+eatSpace(allPCI[10],2)+allPSC[10]+eatSpace(allPSC[10],3)+allBSIC[10]+eatSpace(allBSIC[10],4)+allRNC[10]+eatSpace(allRNC[10],1)+allEnode[10]+eatSpace(allEnode[10],5)+allAzimuth[10]+eatSpace(allAzimuth[10],6)+allCI[10]+eatSpace(allCI[10],7)+allLac[10]+"                  "+allTac[10]);
        m12.setText(allCell[11]+eatSpace(allCell[11],1)+allFrequency[11]+eatSpace(allFrequency[11],1)+allPCI[11]+eatSpace(allPCI[11],2)+allPSC[11]+eatSpace(allPSC[11],3)+allBSIC[11]+eatSpace(allBSIC[11],4)+allRNC[11]+eatSpace(allRNC[11],1)+allEnode[11]+eatSpace(allEnode[11],5)+allAzimuth[11]+eatSpace(allAzimuth[11],6)+allCI[11]+eatSpace(allCI[11],7)+allLac[11]+"                  "+allTac[11]);
        m13.setText(allCell[12]+eatSpace(allCell[12],1)+allFrequency[12]+eatSpace(allFrequency[12],1)+allPCI[12]+eatSpace(allPCI[12],2)+allPSC[12]+eatSpace(allPSC[12],3)+allBSIC[12]+eatSpace(allBSIC[12],4)+allRNC[12]+eatSpace(allRNC[12],1)+allEnode[12]+eatSpace(allEnode[12],5)+allAzimuth[12]+eatSpace(allAzimuth[12],6)+allCI[12]+eatSpace(allCI[12],7)+allLac[12]+"                  "+allTac[12]);
        m14.setText(allCell[13]+eatSpace(allCell[13],1)+allFrequency[13]+eatSpace(allFrequency[13],1)+allPCI[13]+eatSpace(allPCI[13],2)+allPSC[13]+eatSpace(allPSC[13],3)+allBSIC[13]+eatSpace(allBSIC[13],4)+allRNC[13]+eatSpace(allRNC[13],1)+allEnode[13]+eatSpace(allEnode[13],5)+allAzimuth[13]+eatSpace(allAzimuth[13],6)+allCI[13]+eatSpace(allCI[13],7)+allLac[13]+"                  "+allTac[13]);
        m15.setText(allCell[14]+eatSpace(allCell[14],1)+allFrequency[14]+eatSpace(allFrequency[14],1)+allPCI[14]+eatSpace(allPCI[14],2)+allPSC[14]+eatSpace(allPSC[14],3)+allBSIC[14]+eatSpace(allBSIC[14],4)+allRNC[14]+eatSpace(allRNC[14],1)+allEnode[14]+eatSpace(allEnode[14],5)+allAzimuth[14]+eatSpace(allAzimuth[14],6)+allCI[14]+eatSpace(allCI[14],7)+allLac[14]+"                  "+allTac[14]);
        m16.setText(allCell[15]+eatSpace(allCell[15],1)+allFrequency[15]+eatSpace(allFrequency[15],1)+allPCI[15]+eatSpace(allPCI[15],2)+allPSC[15]+eatSpace(allPSC[15],3)+allBSIC[15]+eatSpace(allBSIC[15],4)+allRNC[15]+eatSpace(allRNC[15],1)+allEnode[15]+eatSpace(allEnode[15],5)+allAzimuth[15]+eatSpace(allAzimuth[15],6)+allCI[15]+eatSpace(allCI[15],7)+allLac[15]+"                  "+allTac[15]);
        m17.setText(allCell[16]+eatSpace(allCell[16],1)+allFrequency[16]+eatSpace(allFrequency[16],1)+allPCI[16]+eatSpace(allPCI[16],2)+allPSC[16]+eatSpace(allPSC[16],3)+allBSIC[16]+eatSpace(allBSIC[16],4)+allRNC[16]+eatSpace(allRNC[16],1)+allEnode[16]+eatSpace(allEnode[16],5)+allAzimuth[16]+eatSpace(allAzimuth[16],6)+allCI[16]+eatSpace(allCI[16],7)+allLac[16]+"                  "+allTac[16]);
        m18.setText(allCell[17]+eatSpace(allCell[17],1)+allFrequency[17]+eatSpace(allFrequency[17],1)+allPCI[17]+eatSpace(allPCI[17],2)+allPSC[17]+eatSpace(allPSC[17],3)+allBSIC[17]+eatSpace(allBSIC[17],4)+allRNC[17]+eatSpace(allRNC[17],1)+allEnode[17]+eatSpace(allEnode[17],5)+allAzimuth[17]+eatSpace(allAzimuth[17],6)+allCI[17]+eatSpace(allCI[17],7)+allLac[17]+"                  "+allTac[17]);
        m19.setText(allCell[18]+eatSpace(allCell[18],1)+allFrequency[18]+eatSpace(allFrequency[18],1)+allPCI[18]+eatSpace(allPCI[18],2)+allPSC[18]+eatSpace(allPSC[18],3)+allBSIC[18]+eatSpace(allBSIC[18],4)+allRNC[18]+eatSpace(allRNC[18],1)+allEnode[18]+eatSpace(allEnode[18],5)+allAzimuth[18]+eatSpace(allAzimuth[18],6)+allCI[18]+eatSpace(allCI[18],7)+allLac[18]+"                  "+allTac[18]);
        m20.setText(allCell[19]+eatSpace(allCell[19],1)+allFrequency[19]+eatSpace(allFrequency[19],1)+allPCI[19]+eatSpace(allPCI[19],2)+allPSC[19]+eatSpace(allPSC[19],3)+allBSIC[19]+eatSpace(allBSIC[19],4)+allRNC[19]+eatSpace(allRNC[19],1)+allEnode[19]+eatSpace(allEnode[19],5)+allAzimuth[19]+eatSpace(allAzimuth[19],6)+allCI[19]+eatSpace(allCI[19],7)+allLac[19]+"                  "+allTac[19]);


        d.setContentView(v);
            d.show();


    }


    public String eatSpace(String str, int mode) {

        String space =  createSpace(24);

        if(str.length()==4) {
            space =  createSpace(24);
        }

        else if(str.length()<4){

            if(str.length()==1) {space = createSpace(24)+createSpace((4-str.length())+1);
                return space;
            }


            else space =  createSpace(24)+createSpace((4-str.length()));
        }

        else if(str.length()>4){
            space = createSpace(24).substring(0,24-(str.length()-4)*2);
        }

        return space;

    }


    public String createSpace(int slot) {
        String space = "";

        for(int l = 0; l<slot; l++ )
        {
            space+=" ";
        }

        return space;
    }

    public void showPickSession() {

        if(sessionsIndex!=0) {

            final Dialog d = new Dialog(this);
            d.setTitle("CountyPicker");
            d.setContentView(R.layout.picksession);
            Button b1 = (Button) d.findViewById(R.id.button1);
            Button b2 = (Button) d.findViewById(R.id.button2);
            b1.setBackground(this.getDrawable(R.drawable.testsuite));
            b2.setBackground(this.getDrawable(R.drawable.testsuite));
            final NumberPicker np = (NumberPicker) d.findViewById(R.id.sessionPicker1);

            np.setMaxValue(sessionsIndex-1);
            np.setClickable(false);
            np.setMinValue(0);
            np.setWrapSelectorWheel(false);
            np.setDisplayedValues(helpForDisplay);
            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.dismiss();
                    eraseMarker();
                    getPath(Integer.valueOf(sessions[np.getValue()]));
                    Log.d("poiu", Integer.valueOf(sessions[np.getValue()])+""  );
                }
            });
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("kol", np.getValue() + "");
                    d.dismiss();
                }
            });
            d.show();

        }

        else Toast.makeText(this,"Sessions not found for you mail address!\nPlease check your email setting", Toast.LENGTH_SHORT);
    }

    public void makePath() {

        for(int i = 0; i < pathIndex; i++) {

            if(latPath[i]!=null && longPath[i]!=null) {

                Log.d("badd",latPath[i]+" "+longPath[i]+" "+pathIndex);
                LatLng lg = new LatLng(Double.valueOf(latPath[i]), Double.valueOf(longPath[i]));

                //default TAG
                String bufferTag = "Level : ";
                String bufferValue = "0";
                String bufferTail = "dBm";

                switch(typePath[i]) {

                    case "SPEEDTEST_DL + SPEEDTEST_UL" :
                    {
                        bufferTag = "SpeedTest DL + UL : ";
                        bufferValue = speedPath[i];
                        bufferTail = "kb/s "+speedPathUl[i]+" kb/s";
                        break;
                    }


                    case "SPEEDTEST_DL" :
                        {
                            bufferTag = "SpeedTest Download : ";
                            bufferValue = speedPath[i];
                            bufferTail = "kb/s";
                            break;
                        }

                    case "SPEEDTEST_UL" :
                    {
                        bufferTag = "SpeedTest Upload : ";
                        bufferValue = speedPath[i];
                        bufferTail = "kb/s";
                        break;
                    }

                    case "IDLE_LOG" :
                    {
                        bufferTag = "Level : ";
                        bufferValue = qualPath[i];
                        bufferTail = "dBm";
                        break;
                    }

                    case "PING_LOG" :
                    {
                        bufferTag = "Ping : ";
                        bufferValue = speedPath[i];
                        bufferTail = "ms";
                        break;
                    }

                    case "DATA_DL" :
                    {
                        bufferTag = "Data Test Download : ";
                        bufferValue = speedPath[i];
                        bufferTail = "kb/s";
                        break;
                    }

                    case "DATA_UL" :
                    {
                        bufferTag = "Data Test Upload : ";
                        bufferValue = speedPath[i];
                        bufferTail = "kb/s";
                        break;
                    }

                    case "DROP_LOG" :
                    {
                        bufferTag = "Level : ";
                        bufferValue = qualPath[i];
                        bufferTail = "dBm";
                        break;
                    }

                    case "Dropped Call" :
                    {
                        Log.d("crash", "preso");
                        bufferTag = "Dropped Call ";
                        bufferValue = qualPath[i];
                        bufferTail = "";
                        break;
                    }
                }


                String bufferValueF = bufferValue;

                mMapLayout.addMarker(bufferTag + bufferValueF, lg
                        , bufferTag + bufferValueF + " "+bufferTail
                        , new MarkerOptionsStrategy()
                                .setNormalMarkerOptions(MarkerOptionsFactory
                                        .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), checkWhichColorFilterResource(Integer.valueOf(bufferValue),typePath[i]))
                                                , 15
                                                , checkWhichColorFilter(Integer.valueOf(bufferValue),typePath[i])))
                                //.setHighlightedMarkerOptions(MarkerOptionsFactory.defaultMarkerOptions()));
                                .setHighlightedMarkerOptions(MarkerOptionsFactory
                                        .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), checkWhichColorFilterResource(Integer.valueOf(bufferValue),typePath[i]))
                                                , 15
                                                , checkWhichColorFilter(Integer.valueOf(bufferValue),typePath[i]))));
            }
        }

    }


    public String getSpeedPathColor(int value) {

        if(value == 0)
            return "#000000";

       else if(value < 1000)
            return "#d91e1e";

        else if(value < 3000)
            return "#fc6b14";

        else if(value < 10000)
            return "#002fe0";

        else if(value >= 10000)
            return "#2fff0d";

        else
            return "#ffffff";
    }

    public int checkWhichColorFilterResource(int value, String tag) {

        int field = 0;

        switch(tag){
            case "IDLE_LOG" : {
                field = getPathResourceColor(value);
                break;
            }

            case "DROP_LOG" : {
                field = getPathResourceColor(value);
                break;
            }

            case "DATA_DL" : {
                field = getSpeedPathResourceColor(value);
                break;
            }

            case "SPEEDTEST_DL + SPEEDTEST_UL" : {
                field = getSpeedPathResourceColor(value);
                break;
            }

            case "DATA_UL" : {
                field = getSpeedPathResourceColor(value);
                break;
            }

            case "SPEEDTEST_DL" : {
                field = getSpeedPathResourceColor(value);
                break;
            }

            case "SPEEDTEST_UL" : {
                field = getSpeedPathResourceColor(value);
                break;
            }

            case "Dropped Call" : {
                Log.d("crash","hai capito "+value);
                field = getSpeedPathResourceColor(0);
                break;
            }

            case "PING_LOG": {
                field = getPathResourceColor(value);
                break;
            }

        }

        return field;
    }

    public String checkWhichColorFilter(int value,String tag) {

        String field = "";

        switch(tag){

            case "SPEEDTEST_DL + SPEEDTEST_UL" : {
                field = getSpeedPathColor(value);
                break;
            }

            case "PING_LOG":{
                field = getPathColor(value);
                break;
            }

            case "IDLE_LOG" : {
                field = getPathColor(value);
                break;
            }

            case "DROP_LOG" : {
                field = getPathColor(value);
                break;
            }

            case "DATA_DL" : {
                field = getSpeedPathColor(value);
                break;
            }

            case "DATA_UL" : {
                field = getSpeedPathColor(value);
                break;
            }

            case "SPEEDTEST_DL" : {
                field = getSpeedPathColor(value);
                break;
            }

            case "SPEEDTEST_UL" : {
                field = getSpeedPathColor(value);
                break;
            }

            case "Dropped Call" : {
                field = getSpeedPathColor(value);
                break;
            }

        }

        return field;
    }

    public int getSpeedPathResourceColor(int value){

        if(value == 0)
          return R.drawable.ante4;

        else if(value < 1000)
            return R.drawable.path2;

        else if(value < 3000)
            return R.drawable.path3;

        else if(value < 10000)
            return R.drawable.path4;

        else if(value >= 10000)
            return R.drawable.path5;

          return R.drawable.path1;
    }

    public String getPathColor(int value) {

        if(value < -120)
            return "#000000";

        else if(value < -108)
            return "#d91e1e";

        else if(value < -98)
            return "#fc6b14";

        else if(value < -85)
            return "#002fe0";

        else
            return "#2fff0d";

    }



    public int getPathResourceColor(int value) {

        if(value < -120)
          return R.drawable.path1;

        else if(value < -108)
            return R.drawable.path2;

        else if(value < -98)
            return R.drawable.path3;

        else if(value < -85)
            return R.drawable.path4;

        else
            return R.drawable.path5;

    }


    public void fakePath(){

        final LatLng latLng1 = new LatLng(41.08276665 ,17.0036721);
        final LatLng latLng2 = new LatLng(40.85272745, 17.10699832 );
        final LatLng latLng3 = new LatLng(40.85260555, 17.10707319);
        final LatLng latLng4 = new LatLng(40.85232417, 17.10710239);

        LatLng lg[]  = new LatLng[4];
        lg[0] = latLng1;
        lg[1] = latLng2;
        lg[2] = latLng3;
        lg[3] = latLng4;

        int i = 4;

        for(i = 0; i < 1; i++) {
            mMapLayout.addMarker("pathPnt"+i, lg[i]
                    , "pathPnt"+i
                    , new MarkerOptionsStrategy()
                            .setNormalMarkerOptions(MarkerOptionsFactory
                                    .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante3)
                                            , 5
                                            , "#301235F4"))
                            //.setHighlightedMarkerOptions(MarkerOptionsFactory.defaultMarkerOptions()));
                            .setHighlightedMarkerOptions(MarkerOptionsFactory
                                    .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante3)
                                            , 5
                                            , "#301235F4")));
        }
    }







    public void setupMarker() {
        mMapLayout.setMarkerOptionsStrategy(new MarkerOptionsStrategy()
                .setNormalMarkerOptions(MarkerOptionsFactory
                        .bitmapMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante)))
                .setInforWindowEnabled(false));
    }


    Runnable mrstark = new Runnable() {
        @Override
        public void run() {
            for(int i = 0; i<bufferIndex; i++ ){
                LatLng newloc = new LatLng(lat[i],lng[i]);
                mMapLayout.addMarker(cell[i],newloc,cell[i]); //old but gold.

            }

        }
    };


    public void eraseServMark() {
        if(servMark!=null)
        mMapLayout.removeMarker(servMark);
    }





    public void fillMarkerCla(String pv){

        if(pv!= "---") {
            for (int i = 0; i < bufferIndex; i++) {
                LatLng newloc = new LatLng(lat[i], lng[i]);
                 //old but gold.
                if (provsJson[i] != null && provsJson[i].equals(pv)) {
                    mMapLayout.addMarker(cell[i], newloc
                            , cell[i]
                            , new MarkerOptionsStrategy()
                                    .setNormalMarkerOptions(MarkerOptionsFactory
                                            .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(),R.drawable.ante3)
                                                    , 5
                                                    , "#301235F4"))

                            .setHighlightedMarkerOptions(MarkerOptionsFactory
                                    .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante3)
                                            , 5
                                            , "#301235F4")));
                }
            }
        }

        else {
          //  for (int i = 0; i < bufferIndex; i++) {
          //      LatLng newloc = new LatLng(lat[i], lng[i]);
           //      mMapLayout.addMarker(cell[i],newloc,cell[i]); //old but gold.
           // }
        }
    }

    public void fillMarker() {
        new Thread(new Runnable() {
            public void run(){
          runOnUiThread(mrstark);
            }
        }).start();
    }

    public void eraseMarker() {
            mMapLayout.removeAllMarkers();
    }



    public void fillMe(LatLng newloc) {

        meMark = new Marker("You are here",newloc);

        MarkerOptionsStrategy mk = new MarkerOptionsStrategy()
                .setNormalMarkerOptions(MarkerOptionsFactory
                        .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante)
                                , 5
                                , "#301235F4"))
                .setHighlightedMarkerOptions(MarkerOptionsFactory
                        .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante)
                                , 5
                                , "#301235F4"));

        meMark.setMarkerOptionsStrategy(mk);

        mMapLayout.addMarker(meMark);
    }




    public void fillServ(String cellServing) {

      if(cellServing!=null) {

          for (int i = 0; i < bufferIndex; i++) {

                if (cellServing.contains(cell[i])) {
                   LatLng newloc = new LatLng(lat[i], lng[i]);
                   //mMapLayout.addMarker(cell[i], newloc, cell[i]);
                    servMark = new Marker(cell[i],newloc);
                     mMapLayout.addMarker(cell[i], newloc
                            , cell[i]
                            , new MarkerOptionsStrategy()
                                    .setNormalMarkerOptions(MarkerOptionsFactory
                                            .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante2)
                                                    , 5
                                                    , "#301235F4"))
                                    .setHighlightedMarkerOptions(MarkerOptionsFactory
                                            .bitmapWithHaloMarkerOptions(BitmapFactory.decodeResource(getResources(), R.drawable.ante2)
                                                    , 50
                                                    , "#301235F4")));

                }
          }

       }

    }



    public void showPickCounty() {

        final Dialog d = new Dialog(this);
        d.setCancelable(false);
        d.setTitle("CountyPicker");
        d.setContentView(R.layout.pickcounty);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        b1.setBackground(this.getDrawable(R.drawable.testsuite));
        b2.setBackground(this.getDrawable(R.drawable.testsuite));
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.countyPicker1);
        np.setMaxValue(110);
        np.setClickable(false);
        np.setMinValue(0);
        np.setDisplayedValues(provs);
        np.setWrapSelectorWheel(false);
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
                //set county
                setCounty(np.getValue());
            }
        });
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Log.d("kol",np.getValue()+"");
                d.dismiss();
                redoffsetUpdate();
                startTransitionDown();
            }
        });
        d.show();

    }


    public void setCounty(int county){
        String pvv;
        pvv = provs[county];

        zoomRedoffsetUpdate();
        startTransitionDown();
        eraseServMark();
        fillMarkerCla(pvv);
        fillServ(cellServ);
        fillMe(sd);
    }

    private class CellAPI extends AsyncTask<String, Void, String> {

        protected String doInBackground(final String... urls) {
            //injection ONE SHOT
            Log.d("novio", "provo");
            try {
                URL url2 = new URL("https://www.t-connect.cloud/automation/get_serving.php?ltac="+tac+"&cell="+cellid+"&pci="+pci+"&rat="+rat);
               Log.d("noviod",url2+"");
                HttpURLConnection connb = (HttpURLConnection) url2.openConnection();
                connb.setConnectTimeout(15000);
                connb.setReadTimeout(15000);
                connb.setRequestMethod("GET");
                connb.setRequestProperty("Content-Type", CONTENT_TYPE);
                connb.setRequestProperty("Accept", CONTENT_TYPE);
                //conn.setInstanceFollowRedirects(true);
                connb.connect();
                int result = connb.getResponseCode();
                Log.d("novio", result + "");


                InputStream ins;
                ins = new BufferedInputStream(connb.getInputStream());
                String body = readStream(ins);
                Log.d("noviod", body);
                Log.d("noviod",ins+"");
                retriveCell(body);
            } catch (Exception ec) {
                Log.e("maronno", ec.getMessage() + "");
            }
            return "ok";
        }


        @Override
        protected void onPostExecute(String response) {

        }

    }


    static String readStream(InputStream in) {
        java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private class InfoAPI extends AsyncTask<String, Void, String> {

        protected String doInBackground(final String... urls) {
            //injection ONE SHOT
            Log.d("novio", "provo");
            try {
                URL url2 = new URL("https://www.t-connect.cloud/automation/get_serving_info2.php?cell="+urls[0]);
                Log.d("noviod",url2+"");
                HttpURLConnection connb = (HttpURLConnection) url2.openConnection();
                connb.setConnectTimeout(20000);
                connb.setReadTimeout(20000);
                connb.setRequestMethod("GET");
                connb.setRequestProperty("Content-Type", CONTENT_TYPE);
                connb.setRequestProperty("Accept", CONTENT_TYPE);
                //conn.setInstanceFollowRedirects(true);
                connb.connect();
                int result = connb.getResponseCode();
                Log.d("novio", result + "");

                InputStream ins;
                ins = new BufferedInputStream(connb.getInputStream());
                String body = readStream(ins);
                Log.d("noviod", body);
                Log.d("noviod",ins+"");
                retriveInfo(body);
            } catch (Exception ec) {
                Log.e("maronno", ec.getMessage() + "");
            }
            return "ok";
        }


        @Override
        protected void onPostExecute(String response) {

        }

    }


    private class PathAPI extends AsyncTask<String, Void, String> {

        protected String doInBackground(final String... urls) {
            //injection ONE SHOT
            Log.d("novio", "https://www.t-connect.cloud/automation/yon_log_path.php?session="+urls[0]+"&token="+TestService.token);
            try {
                URL url2 = new URL("https://www.t-connect.cloud/automation/yon_log_path.php?session="+urls[0]+"&token="+TestService.token);
                HttpURLConnection connb = (HttpURLConnection) url2.openConnection();
                connb.setConnectTimeout(5000);
                connb.setReadTimeout(5000);
                connb.setRequestMethod("GET");
                connb.setRequestProperty("Content-Type", CONTENT_TYPE);
                connb.setRequestProperty("Accept", CONTENT_TYPE);
                //conn.setInstanceFollowRedirects(true);
                connb.connect();
                int result = connb.getResponseCode();
                Log.d("novio", result + "");


                InputStream ins = null;
                ins = new BufferedInputStream(connb.getInputStream());
                String body = readStream(ins);
                Log.d("novio", body);
                retriveJSONPath(body);
            } catch (Exception ec) {
                Log.e("maronno", ec.getMessage() + "");
            }
            return "ok";
        }


        @Override
        protected void onPostExecute(String response) {

        }

    }




    private class SessionsAPI extends AsyncTask<String, Void, String> {

        protected String doInBackground(final String... urls) {
            //injection ONE SHOT
            Log.d("novio", "https://www.t-connect.cloud/automation/logs.php?token="+ TestService.token);
            try {
                URL url2 = new URL("https://www.t-connect.cloud/automation/logs.php?token="+ TestService.token);
                HttpURLConnection connb = (HttpURLConnection) url2.openConnection();
                connb.setConnectTimeout(5000);
                connb.setReadTimeout(5000);
                connb.setRequestMethod("GET");
                connb.setRequestProperty("Content-Type", CONTENT_TYPE);
                connb.setRequestProperty("Accept", CONTENT_TYPE);
                //conn.setInstanceFollowRedirects(true);
                connb.connect();
                int result = connb.getResponseCode();
                Log.d("novio", result + "");


                InputStream ins = null;
                ins = new BufferedInputStream(connb.getInputStream());
                String body = readStream(ins);
                Log.d("novio", body);
                retriveJSONSessions(body);
            } catch (Exception ec) {
                Log.e("maronno", ec.getMessage() + "");
            }
            return "ok";
        }


        @Override
        protected void onPostExecute(String response) {

        }

    }



    private class RadiosAPI extends AsyncTask<String, Void, String> {

        protected String doInBackground(final String... urls) {
            //injection ONE SHOT
            Log.d("novio", "provo");
            try {
                URL url2 = new URL("https://www.t-connect.cloud/automation/getmapsn.php");
                HttpURLConnection connb = (HttpURLConnection) url2.openConnection();
                connb.setConnectTimeout(5000);
                connb.setReadTimeout(5000);
                connb.setRequestMethod("GET");
                connb.setRequestProperty("Content-Type", CONTENT_TYPE);
                connb.setRequestProperty("Accept", CONTENT_TYPE);
                //conn.setInstanceFollowRedirects(true);
                connb.connect();
                int result = connb.getResponseCode();
                Log.d("novio", result + "");


                InputStream ins = null;
                ins = new BufferedInputStream(connb.getInputStream());
                String body = readStream(ins);
                Log.d("novio", body);
                retriveJSONRadios(body);
            } catch (Exception ec) {
                Log.e("maronno", ec.getMessage() + "");
            }
            return "ok";
        }


        @Override
        protected void onPostExecute(String response) {

        }

        }

    public void getCell() {

        CellAPI api = new CellAPI();
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "foo");
    }

    public void getSessions() {

        SessionsAPI api = new SessionsAPI();
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "foo");
    }


    public void getPath(int value) {

        PathAPI api = new PathAPI();
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, value+"");
    }


    public void getRadios() {

        RadiosAPI api = new RadiosAPI();
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "foo");
    }


    public void getInfos(String name) {

        InfoAPI api = new InfoAPI();
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, name);
    }



    public void retriveJSONRadios(String body) {

        Log.d("novio","ok");

        try {

            JSONArray jArray = new JSONArray(body);
            bufferIndex = jArray.length();

            //max cap for cells number
            if(bufferIndex>24000) bufferIndex = 24000;

            for(int i = 0; i < bufferIndex; i++){
                try {
                    cell[i] = jArray.getJSONObject(i).getString("CELL");
                    if(!cell[i].contains("CH069") ) {
                        lat[i] = jArray.getJSONObject(i).getDouble("LAT");
                        lng[i] = jArray.getJSONObject(i).getDouble("LNG");
                        provsJson[i] = jArray.getJSONObject(i).getString("PROV");
                        Log.d("TYRANT",provsJson[i] + "  :"+ i );
                    }
                }
                catch (Exception e) {
                    Log.d("noivo","one skipped");
                    Log.d("noivo",e.getMessage()+"");
                }
            }






        } catch (JSONException e) {
            Log.d("maronno","auth : "+e.getMessage()+"");
        }


        //finished so its time for initmock
        runOnUiThread(init);
    }


    public void retriveJSONPath(String body) {

        try {

            JSONArray jArray = new JSONArray(body);
            pathIndex = jArray.length();

            for(int i = 0; i < pathIndex; i++){
                try {
                    qualPath[i] = jArray.getJSONObject(i).getString("rsrp");
                    latPath[i] = jArray.getJSONObject(i).getString("lat");
                    longPath[i] = jArray.getJSONObject(i).getString("long");
                    speedPath[i] = jArray.getJSONObject(i).getString("data_speed");

                    typePath[i] =  jArray.getJSONObject(i).getString("log_type");



                    String speedDl = jArray.getJSONObject(i).getString("data_speed_dl");
                    String speedUl = jArray.getJSONObject(i).getString("data_speed_ul");

                    if(speedDl!=null && !speedDl.equals("0")){
                        speedPath[i] = speedDl;
                    }

                    if(speedUl!=null && !speedUl.equals("0")){
                        speedPathUl[i] = speedUl;
                    }

                    if(jArray.getJSONObject(i).getBoolean("dropped")==true){
                        Log.d("crash","ci siamo");
                        typePath[i] = "Dropped Call";
                    }
                }

                catch (Exception e) {
                    Log.d("noivo","one skipped");
                    Log.d("noivo",e.getMessage()+"");
                }
            }



        } catch (JSONException e) {
            Log.d("maronno","go : "+e.getMessage()+"");
        }

        runOnUiThread(fillPath);

    }






    public void retriveJSONSessions(String body) {

        try {

            JSONArray jArray = new JSONArray(body);
            sessionsIndex = jArray.length();

            if(sessionsIndex>3000){
                sessionsIndex=3000;
            }

            helpForDisplay = new String[sessionsIndex];

            for(int i = 0; i < sessionsIndex; i++){

                try {
                    sessions[i] = jArray.getJSONObject(i).getString("log_id");
                    date[i] = jArray.getJSONObject(i).getString("date");
                    time[i] = jArray.getJSONObject(i).getString("time");

                    helpForDisplay[i] = sessions[i]+" | "+date[i]+" | "+time[i];
                    Log.d("novis",helpForDisplay[i]);
                }

                catch (Exception e) {
                    Log.d("noivo","one skipped");
                    Log.d("noivo",e.getMessage()+"");
                }
            }



        } catch (JSONException e) {
            Log.d("maronno","go : "+e.getMessage()+"");
        }

        Log.d("supersessi",sessionsIndex+" "+sessions[2]);

    }





    public void retriveInfo(String body) {

       // cellInfo = body;

        try {

            JSONArray jsonArray = new JSONArray(body);
            int lenght = jsonArray.length();
            globalLenght = lenght;


            for(int i=0; i<lenght; i++) {

                cellInfo = "Cell Name : " +jsonArray.getJSONObject(i).getString("cell").substring(0,5) + "\n";
                cellInfo += "Vendor : " + jsonArray.getJSONObject(i).getString("vendor")+"\n";

                cellInfo += "Indirizzo : " + jsonArray.getJSONObject(i).getString("indirizzo");
                cellInfo += " "+jsonArray.getJSONObject(i).getString("comune") + "\n";

                allAzimuth[i] = jsonArray.getJSONObject(i).getString("azimuth");
                allLac[i] = jsonArray.getJSONObject(i).getString("lac");
                allFrequency[i] = jsonArray.getJSONObject(i).getString("band");
                allCell[i] = jsonArray.getJSONObject(i).getString("cell");
                allPCI[i] = jsonArray.getJSONObject(i).getString("pci");
                allPSC[i] = jsonArray.getJSONObject(i).getString("primary_sc");
                allBSIC[i] = jsonArray.getJSONObject(i).getString("bsic");
                allRNC[i] = jsonArray.getJSONObject(i).getString("rncid");
                allCI[i] = jsonArray.getJSONObject(i).getString("ci");
                allEnode[i] = jsonArray.getJSONObject(i).getString("enodebid");
                allTac[i] = jsonArray.getJSONObject(i).getString("tac");

                if(allFrequency[i].equals("")) allFrequency[i] = "NULL";
                if(allPCI[i].equals("")) allPCI[i] = "NULL";
                if(allPSC[i].equals("")) allPSC[i] = "NULL";
                if(allBSIC[i].equals("")) allBSIC[i] = "NULL";
                if(allRNC[i].equals("")) allRNC[i] = "NULL";
                if(allEnode[i].equals("")) allEnode[i] = "NULL";
                if(allAzimuth[i].equals("")) allAzimuth[i] = "NULL";
                if(allCI[i].equals("")) allCI[i] = "NULL";
                if(allLac[i].equals("")) allLac[i] = "NULL";
                if(allTac[i].equals("")) allTac[i] = "NULL";

                for(int p = 0; p<20; p++){
                    if(allCell[p]==null) {
                        allCell[p] = "";
                        allFrequency[p] = "";
                        allPCI[p] = "";
                        allPSC[p] = "";
                        allBSIC[p] = "";
                        allRNC[p] = "";
                        allEnode[p] = "";
                        allAzimuth[p] = "";
                        allCI[p] = "";
                        allLac[p] = "";
                        allTac[p] = "";
                    }

                }



            }

            cellInfo +=  "Tac :" + discardVec(allTac, lenght)+"\n";
            cellInfo +=  "Lac :" + discardVec(allLac, lenght)+"\n";
            cellInfo +=  "Frequenze :" + discardVec(allFrequency, lenght)+"\n";
            cellInfo +=  "Azimuth :" + discardVec(allAzimuth, lenght)+"\n";

        } catch (JSONException e) {
            Log.d("MANDA",e.getMessage()+"");
        }


        //injection
        //,,,,



        runOnUiThread(fillInfo);
    }


    public String discardVec(String[] disc, int lenght) {

        Set<String> myset  = new HashSet<String>();
        Collections.addAll(myset,disc);

        String ok = "";
        String[] listofWords = myset.toArray(new String[myset.size()]);
        for(int i = 0; i<myset.size(); i++){
            if(listofWords[i]!=null && !listofWords[i].equalsIgnoreCase("null"))
            ok += " "+ listofWords[i];
        }
          return ok;
    }





    public void retriveCell(String body) {

        body = body.replaceAll("\\s+","");

        int cg = body.length();
            cellServ = body.substring(1,body.length()-3);
            //injnection
            //,,,,
            runOnUiThread(fill);
    }

    @Override
    protected void onStop() {

        super.onStop();
        if(mLocationManager!=null && mLocationListener!= null) mLocationManager.removeUpdates(mLocationListener);

    }


    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Press EXIT button to leave application", Toast.LENGTH_SHORT).show();
    }



    public void initMock() {

        mMapLayout.getMap().getUiSettings().setCompassEnabled(false);
        mMapLayout.getMap().setBuildingsEnabled(true);

                mMapLayout.updateCamera(true);
                startInitialUpdate();

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    Runnable picker = new Runnable() {
        @Override
        public void run() {
            showPickCounty();
        }
    };


    Runnable changeLegend3 = new Runnable() {
        @Override
        public void run() {

        }
    };

    Runnable changeLegend2 = new Runnable() {
        @Override
        public void run() {

        }
    };

    Runnable changeLegend = new Runnable() {
        @Override
        public void run() {

        }
    };

    Runnable init = new Runnable() {
        @Override
        public void run() {
            initMock();
        }
    };


    Runnable fillInfo = new Runnable() {
        @Override
        public void run() {
            infos.setText(cellInfo+"");


        }
    };


    Runnable fillPath = new Runnable() {
        @Override
        public void run() {
            makePath();
        }
    };


    Runnable clickmarker = new Runnable() {
        @Override
        public void run() {
            mMapLayout.getMap().animateCamera(markerUpdate);
        }
    };


    Runnable fill = new Runnable() {
        @Override
        public void run() {
            fillServ(cellServ);
        }
    };

    @SuppressLint("MissingPermission")
    public void startUpdate() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 200,
                15, mLocationListener);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100,
                10, mLocationListener);
    }


    public void startTransitionDown() {

        ObjectAnimator animation = ObjectAnimator.ofFloat(mGrid, "translationY", dpToPx(0,getApplicationContext()));
        animation.setDuration(600);
        animation.start();

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(mCounty, "translationY", dpToPx(0,getApplicationContext()));
        animation2.setDuration(600);
        animation2.start();

        ObjectAnimator animation4 = ObjectAnimator.ofFloat(infos, "translationY", dpToPx(-0,getApplicationContext()));
        animation4.setDuration(600);
        animation4.start();

        ObjectAnimator animation4b = ObjectAnimator.ofFloat(mMode, "translationY", dpToPx(-0,getApplicationContext()));
        animation4b.setDuration(600);
        animation4b.start();

        ObjectAnimator animation5 = ObjectAnimator.ofFloat(hook, "translationY", dpToPx(0,getApplicationContext()));
        animation5.setDuration(600);
        animation5.start();

        ObjectAnimator animation6 = ObjectAnimator.ofFloat(whiteboard, "translationY", dpToPx(0,getApplicationContext()));
        animation6.setDuration(600);
        animation6.start();

        ObjectAnimator animation7 = ObjectAnimator.ofFloat(mToggle, "translationY", dpToPx(0,getApplicationContext()));
        animation7.setDuration(600);
        animation7.start();

        ObjectAnimator animation8 = ObjectAnimator.ofFloat(mLegend, "translationY", dpToPx(0,getApplicationContext()));
        animation8.setDuration(900);
        animation8.start();
    }


    public void startTransitionUp() {

        ObjectAnimator animation = ObjectAnimator.ofFloat(mGrid, "translationY", dpToPx(-350,getApplicationContext()));
        animation.setDuration(600);
        animation.start();

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(mCounty, "translationY", dpToPx(-350,getApplicationContext()));
        animation2.setDuration(600);
        animation2.start();

        ObjectAnimator animation4 = ObjectAnimator.ofFloat(infos, "translationY", dpToPx(-350,getApplicationContext()));
        animation4.setDuration(600);
        animation4.start();

        ObjectAnimator animation4b = ObjectAnimator.ofFloat(mMode, "translationY", dpToPx(-350,getApplicationContext()));
        animation4b.setDuration(600);
        animation4b.start();

        ObjectAnimator animation5 = ObjectAnimator.ofFloat(hook, "translationY", dpToPx(-350,getApplicationContext()));
        animation5.setDuration(600);
        animation5.start();

        ObjectAnimator animation6 = ObjectAnimator.ofFloat(whiteboard, "translationY", dpToPx(-350,getApplicationContext()));
        animation6.setDuration(600);
        animation6.start();

        ObjectAnimator animation7 = ObjectAnimator.ofFloat(mToggle, "translationY", dpToPx(-410,getApplicationContext()));
        animation7.setDuration(700);
        animation7.start();

        ObjectAnimator animation8 = ObjectAnimator.ofFloat(mLegend, "translationY", dpToPx(100,getApplicationContext()));
        animation8.setDuration(600);
        animation8.start();
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {

            LatLng sd2 = new LatLng(location.getLatitude(),location.getLongitude());
            sd = sd2;
            if(meMark!=null) mMapLayout.removeMarker(meMark);
            fillMe(sd2);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(sd, 10);
            GoogleMap map = mMapLayout.getMap();
            if(map!=null && sd!=null) {
                Log.d("nani", "we got the map");
                map.animateCamera(yourLocation);
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    public void startInitialUpdate(){

        Log.d("bastard","hhh");
        LatLng sd2 = new LatLng(41.9109, 12.4818);
        sd = sd2;
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(sd, 10);
        GoogleMap map = mMapLayout.getMap();

        if(map!=null && sd!=null) {
            Log.d("nani", "we got the map");
            map.animateCamera(yourLocation);
        }

        //TODO : check null
        tac = Integer.valueOf(TestService.tac);
        rat = Integer.valueOf(TestService.rat);

        if(rat==4) {
            cellid = Integer.valueOf(TestService.worked);
        } else {
            cellid = Integer.valueOf(TestService.ci);
        }
        pci = Integer.valueOf(TestService.pci);

        // setupMarker(); no customs for now.

        getCell();

        // fillMarker(); we prefer to enable this by button

        //..and start update for location
        startUpdate();
    }

}
