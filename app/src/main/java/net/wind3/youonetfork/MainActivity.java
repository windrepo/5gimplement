package net.wind3.youonetfork;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.anastr.speedviewlib.SpeedView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.ramotion.circlemenu.CircleMenuView;


import net.wind3.youonetfork.layout.ButtonMulti;
import net.wind3.youonetfork.layout.ButtonPath;
import net.wind3.youonetfork.layout.ButtonPing;
import net.wind3.youonetfork.layout.ButtonStart;
import net.wind3.youonetfork.layout.ButtonStop;
import net.wind3.youonetfork.layout.ButtonST;
import net.wind3.youonetfork.layout.ButtonIdle;
import net.wind3.youonetfork.layout.ButtonUL;
import net.wind3.youonetfork.layout.ButtonDL;
import net.wind3.youonetfork.layout.ButtonConfirm;
import net.wind3.youonetfork.layout.ButtonGoBack;
import net.wind3.youonetfork.layout.ButtonCall;

import net.wind3.youonetfork.services.TestService;
import net.wind3.youonetfork.task.TaskOkHttp;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    /**
     * -- youONet v1.9.2 --
     * Attention : This app was made in two weeks. i was tasked
     * to make an iOS/Android version as a replacement for another project
     * that was scrapped after 6 months of development. Bear with me
     * if something is not textbook consistent.
     *
     */


    //UI navigation button
    ImageView mOptions;
    ImageView mUnsub;
    BottomNavigationView navigation;

    //SPEED GAUGE
    public SpeedView speedView;

    //fx-flashing
    boolean testFx = false;
    float alpha = 0;
    boolean goAlpha = false;

    //Loading gif
    boolean shouldUpdateIcon = true;

    //hash map vars
    private Map<String, String> georefM = null;
    private String cachedLac = "";

    //Save file
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    //Account mail & password
    String mail = "myemail@example.com";
    String pass = "";
    String responseLogin = "";
    String disclaimer = "";
    boolean shouldSkinAuth = false;


    //Buttons
    Button archdown;
    Button archtoogle;
    Button acceptDisclaimer;

    //Lock UI
    boolean lock = false;
    boolean completed = false;

    EditText loginMail;
    EditText loginPass;
    Button loginSet;

    //Buttons Test
    ButtonPath mMapPath;
    // ButtonIdle mB1;
    // ButtonDL mB2;
    // ButtonUL mB3;
    // ButtonST mB4;
    // ButtonCall mB5;
    // ButtonMulti mB6;
    // ButtonPing mB7;

    ButtonConfirm mConfirm;
    ButtonGoBack mCancel;
    ButtonStop mStop;

    CircleMenuView menu;


    //GifImageView pendingTest;
    ButtonStart pendingTest;
    ImageView stepbar;
    public TextView displayState;
    public TextView displayLogin;

    //Views
    TextView mFreq;
    TextView mFreq_title;
    TextView mCell;
    TextView mCell_title;
    TextView mPci;
    TextView mPci_title;
    TextView mTac;
    TextView mTac_title;
    TextView mAvDL;
    TextView mAvDL_title;
    TextView mAvUL;
    TextView mAvUL_title;
    TextView mEnb;
    TextView mPci5G;
    TextView mPci5G_title;
    TextView mEnb_title;
    TextView mLevel;
    TextView mLevel_title;
    TextView mQual;
    TextView mQual_title;
    TextView mRat;
    TextView mRat_title;
    TextView mGeo_title;
    TextView mGeo;
    View line1;
    View line2;
    ImageView mLogo;

    TextView startTitle;

    //views vector for easy animation management
    View[] mViews;
    View[] mButtons;
    View[] mDual;

    private LineChart chart;

    //reading toogle
    boolean isReadActive = false;
    ArrayList<Entry> values = new ArrayList<>();

    //Telephony manager
    TelephonyManager tm;

    //reading vars for UI toogle
    String label = "";

    //Test TYPE
    int testTypeUI = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_permissions);

        EditText mailSet = findViewById(R.id.editMail);
        Button buttonMailSet = findViewById(R.id.buttonMail);

        mailSet.setEnabled(false);
        buttonMailSet.setEnabled(false);
        mailSet.setAlpha(0f);
        buttonMailSet.setAlpha(0f);

        /* Application entry point, let's go. */

        //remove title bar..and init framework rendering flags
        getWindow().setStatusBarColor(Color.rgb(189, 212, 227));

        /* Initial for localization, this was scrapped after two days */
        // Locale locale = new Locale("en");
        // Configuration configuration = getBaseContext().getResources().getConfiguration();
        // configuration.setLocale(locale);


        final int lFlags = getWindow().getDecorView().getSystemUiVisibility();
        getWindow().getDecorView().setSystemUiVisibility(true ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));

        getSupportActionBar().hide();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        requestPermission();

    }


    public void lifeSaver(){

        if(TestService.isAppending){
            whichTestAppending();
            Log.d("happenz","i am appending");
        }

        else {

            if(TestService.screen==2){
                whichTestChoice();
                Log.d("happenz","choices");
                return;
            }


            if(TestService.screen==1){
                prepareToSelectTest();
                Log.d("happenz","main");
                return;
            }

        }

    }


    public void whichTestAppending(){

        switch(TestService.testType){
            case 1:
                resumeforTest(0);
                break;
            case 2:
                resumeforTest(1);
                break;
            case 3:
                resumeforTest(2);
                break;
            case 4:
                resumeforTest(3);
                break;
            case 5:
                resumeforTest(4);
                break;
        }
    }


    public void resumeforTest(int test) {
        hideMasterButton();

        testTypeUI = TestService.testType;
        switch(testTypeUI) {
            case 1 : label = "IDLE";  break;
            case 2 : label = "DATA DL";  break;
            case 3 : label = "DATA UP"; break;
            case 4 : label = "SPEED"; break;
            case 5 : label = "CALL"; break;
            case 6 : label = "PING"; break;
            case 7 : label = "MULTIRAB"; break;
        }
        nowResuming();
    }

    public void whichTestChoice(){

        switch(TestService.testType){
            case 1:
                choiceforTest(0);
                break;
            case 2:
                choiceforTest(1);
                break;
            case 3:
                choiceforTest(2);
                break;
            case 4:
                choiceforTest(3);
                break;
            case 5:
                choiceforTest(4);
                break;
            case 6:
                choiceforTest(5);
                break;
            case 7:
                choiceforTest(6);
                break;

        }
    }


    public void choiceforTest(int test) {
        hideMasterButton();
        hideTestSelects(test);
    }

    public void initSaveFile() {
        pref = getSharedPreferences("net.wind3.youonetfork.PREFERENCES_FILE", Context.MODE_PRIVATE);
    }

    public void saveLogin(){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("pass", pass);
        editor.putString("email", mail);
        editor.commit();
    }

    public void saveSkypeCheck(String level){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("skypeCheck", level);
        editor.commit();
    }


    public void saveDisclaimer(){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("disclaimer", disclaimer);
        editor.commit();
    }

    protected  void goToMailsetter() {
        TextView messageFormail = findViewById(R.id.displayPermissions);
        messageFormail.setText("Insert a valid email\nfor account registration.");


        final EditText mailSet = findViewById(R.id.editMail);

        final Button buttonMailSet = findViewById(R.id.buttonMail);
        mailSet.setEnabled(true);
        buttonMailSet.setEnabled(false);
        mailSet.setAlpha(1f);
        buttonMailSet.setAlpha(1f);

        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().contains("@") && s.length()>3){
                    buttonMailSet.setEnabled(true);
                    buttonMailSet.setBackground(getResources().getDrawable(R.drawable.squareconfirmgreen));
                    buttonMailSet.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                else {
                    buttonMailSet.setEnabled(false);
                    buttonMailSet.setBackground(getResources().getDrawable(R.drawable.squareconfirmred));
                    buttonMailSet.setTextColor(Color.WHITE);
                }

            }
        };

        mailSet.addTextChangedListener(tw);

        buttonMailSet.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        buttonMailSet.setBackground(getResources().getDrawable(R.drawable.squareconfirmcyan));
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mail = mailSet.getText().toString();
                        goToDisclaimer();
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });

    }


    public void showMap() {
        MapActivity.setMasterPtr(this);
        Intent MapActivityIntent = new Intent(this, MapActivity.class);
        startActivity(MapActivityIntent);
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    public void goToLogin() {

        setContentView(R.layout.activity_login);

        displayLogin = findViewById(R.id.displayLogin);

        loginMail = findViewById(R.id.loginMail);
        loginPass = findViewById(R.id.loginPass);
        loginSet = findViewById(R.id.buttonLogin);

        loginSet.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        loginSet.setBackground(getResources().getDrawable(R.drawable.squareconfirmcyan));
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        pass = loginPass.getText().toString();
                        mail = loginMail.getText().toString();
                        saveLogin();
                        checkLoginResponse();
                        //goToMainActivity();..go to login, instead.
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        //check if we've have stored our access credentials

        pass = pref.getString("pass","nothing");
        mail = pref.getString("email","nothing");

        if(!pref.getString("email","nothing").equals("nothing") &&
                !pref.getString("pass","nothing").equals("nothing")){
            displayLogin.setText("Connecting..");
            loginSet.setAlpha(0f);
            loginMail.setAlpha(0f);
            loginPass.setAlpha(0f);
            //loginSet.setBackground(getResources().getDrawable(R.drawable.squareconfirmcyan));
            checkLoginResponse();
        }


        //goToMainActivity();
    }


    protected void goToDisclaimer() {

        LayoutInflater inflator=getLayoutInflater();
        View view4=inflator.inflate(R.layout.activity_disclaimer, null, false);
        view4.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in));

        setContentView(view4);

        //setContentView(R.layout.activity_disclaimer);
        acceptDisclaimer = findViewById(R.id.squareconfirm);

        acceptDisclaimer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        acceptDisclaimer.setBackground(getResources().getDrawable(R.drawable.squareconfirmcyan));
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {

                        disclaimer = "accepted";
                        saveDisclaimer();
                        //we we're saving mail here, after accepting disclaimer

                        shouldSkinAuth = TestService.shouldSkip;
                        if(shouldSkinAuth) {
                            goToMainActivity();
                        }
                        else goToLogin();
                        //goToMainActivity();..go to login, instead.
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });

    }


    public void goToMainActivity() {

        setContentView(R.layout.activity_main_dev);

        //remove title bar..and init framework rendering flags
        getWindow().setStatusBarColor(Color.rgb(189, 212, 227));

        final int lFlags = getWindow().getDecorView().getSystemUiVisibility();
        getWindow().getDecorView().setSystemUiVisibility(true ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));

        getSupportActionBar().hide();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //TODO : all buttons..

        menu = (CircleMenuView) findViewById(R.id.circle_menu);
        menu.setScaleX(1.5f);
        menu.setScaleY(1.5f);
        //  menu.setIconMenu(R.drawable.idle);
        //  menu.invalidate();
        menu.setEventListener(new CircleMenuView.EventListener() {
            @Override
            public void onMenuOpenAnimationStart(@NonNull CircleMenuView view) {
                Log.d("D", "onMenuOpenAnimationStart");
            }

            @Override
            public void onMenuOpenAnimationEnd(@NonNull CircleMenuView view) {
                Log.d("D", "onMenuOpenAnimationEnd");
            }

            @Override
            public void onMenuCloseAnimationStart(@NonNull CircleMenuView view) {
                Log.d("D", "onMenuCloseAnimationStart");
            }

            @Override
            public void onMenuCloseAnimationEnd(@NonNull CircleMenuView view) {
                Log.d("D", "onMenuCloseAnimationEnd");
            }

            @Override
            public void onButtonClickAnimationStart(@NonNull CircleMenuView view, int index) {
                Log.d("D", "onButtonClickAnimationStart| index: " + index);
            }

            @Override
            public void onButtonClickAnimationEnd(@NonNull CircleMenuView view, int index) {
                clickItem(index);
                view.setEnabled(false);
            }
        });

        mUnsub = findViewById(R.id.unsub);

        mUnsub.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {

                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                    {
                        unsubAlert();
                    }
                    // RELEASED
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });


        mMapPath = findViewById(R.id.pathbutton);
        // mB1 = findViewById(R.id.test1);
        // mB2 = findViewById(R.id.test2);
        // mB3 = findViewById(R.id.test3);
        // mB4 = findViewById(R.id.test4);
        // mB5 = findViewById(R.id.test5);
        // mB6 = findViewById(R.id.test6);
        // mB7 = findViewById(R.id.test7);

        mConfirm = findViewById(R.id.confirm);

        mConfirm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mConfirm.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mConfirm.changeBackgroundToBump();
                        if(TestService.screen==2 && lock == false){
                            // checkForDefaultDialer();
                            lock = true;
                            hideConfirmCancel(0);
                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });


        mStop = findViewById(R.id.stop);

        mStop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        if(!completed)
                            mStop.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        if(!completed) {
                            prepareToSending();
                        }
                        else {
                            shouldUpdateIcon = false;
                            hideGauge();
                            finishedResetButton();
                            prepareToSelectTest();
                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });



        mCancel = findViewById(R.id.goback);

        mCancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mCancel.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mCancel.changeBackgroundToBump();
                        if(TestService.screen==2 && lock == false){
                            lock = true;
                            hideConfirmCancel(1);
                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });


        archdown = findViewById(R.id.archreader);
        archdown.setBackgroundResource(R.drawable.archread);

        archdown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                turnReadingOff();
                isReadActive=false;

            }
        });



        archtoogle = findViewById(R.id.toogle);

        archtoogle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(isReadActive)
                {
                    turnReadingOff();
                    isReadActive=false;
                }

                else
                {
                    turnReadingOn();
                    isReadActive = true;
                    startReading();
                }
            }
        });


        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.action_item2:
                        final Handler handler2 = new Handler();

                        Runnable runnable2 = new Runnable() {
                            @Override
                            public void run() {
                                showMap();
                                handler2.removeCallbacksAndMessages(null);
                            }
                        };

                        handler2.postDelayed(runnable2,300);

                        break;

                    case R.id.action_item3:
                        exitAlert();
                        break;
                }
                return true;
            }
        });


        mMapPath.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mMapPath.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {

                        mMapPath.changeBackgroundToBump();
                        String url = TestService.urlMap;
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);

                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });


/*
        mB1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mB1.changeBackgroundToPress();
                    }
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        Log.d("mb1",TestService.screen+"");
                        mB1.changeBackgroundToBump();
                        if(TestService.screen==1 && lock==false){

                            boolean gps_enabled = false;

                            try {
                                gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            } catch(Exception ex) {}

                               if(gps_enabled) {
                                   //seelct test idle
                                   TestService.testType = 1;
                                   lock = true;
                                   TestService.screen = 2;
                                   hideTestSelects(0);
                               }

                               else{
                                   Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                               }

                                  }
                    }
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });


        mB2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mB2.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mB2.changeBackgroundToBump();
                        if(TestService.screen==1 && lock == false){

                            boolean gps_enabled = false;

                            try {
                                gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            } catch(Exception ex) {}

                            if(gps_enabled) {
                                //seelct test data dl
                                TestService.testType = 2;
                                lock = true;
                                TestService.screen = 2;
                                hideTestSelects(1);
                            }

                            else{
                                Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });



        mB3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mB3.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mB3.changeBackgroundToBump();
                        if(TestService.screen==1 && lock == false){

                            boolean gps_enabled = false;

                            try {
                                gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            } catch(Exception ex) {}

                            if(gps_enabled) {
                                //seelct test data ul
                                TestService.testType = 3;
                                lock = true;
                                TestService.screen = 2;
                                hideTestSelects(2);
                            }

                            else{
                                Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });




        mB4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mB4.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mB4.changeBackgroundToBump();
                        if(TestService.screen==1 && lock == false){

                            boolean gps_enabled = false;

                            try {
                                gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            } catch(Exception ex) {}


                            if(gps_enabled) {
                                //seelct test speedtest
                                TestService.testType = 4;
                                lock = true;
                                TestService.screen = 2;
                                hideTestSelects(3);
                            }

                            else{
                                Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });


        mB5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                    {
                        mB5.changeBackgroundToPress();
                    }
                    return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                    {
                        mB5.changeBackgroundToBump();
                        if(TestService.screen==1 && lock == false){

                            boolean gps_enabled = false;

                            try {
                                gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            } catch(Exception ex) {}

                            if(gps_enabled) {
                                //seelct test drop
                                TestService.testType = 5;
                                lock = true;
                                TestService.screen = 2;
                                hideTestSelects(4);
                            }

                            else{
                                Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        */

        displayState = findViewById(R.id.displayState);
        stepbar = findViewById(R.id.stepbar);

        pendingTest = findViewById(R.id.loading5);
        pendingTest.setEnabled(false);
        pendingTest.setAlpha(0f);

        pendingTest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(TestService.screen==0 && lock==false) {
                    //dummy
                }
            }
        });


        mLogo = findViewById(R.id.logo);
        mFreq = findViewById(R.id.freq);
        mFreq_title = findViewById(R.id.freq_title);

        mCell = findViewById(R.id.cell);
        mCell_title = findViewById(R.id.cell_title);
        mRat = findViewById(R.id.rat);
        mRat_title = findViewById(R.id.rat_title);
        mQual = findViewById(R.id.qual);
        mQual_title = findViewById(R.id.qual_title);
        mLevel = findViewById(R.id.level);
        mLevel_title = findViewById(R.id.level_title);
        mEnb = findViewById(R.id.enode);
        mEnb_title = findViewById(R.id.enode_title);
        mTac = findViewById(R.id.tac);
        mTac_title = findViewById(R.id.tac_title);
        mPci = findViewById(R.id.pci);
        mPci_title = findViewById(R.id.pci_title);

        mPci5G = findViewById(R.id.pci5G);
        mPci5G_title = findViewById(R.id.pci5g_title);


        mAvUL_title = findViewById(R.id.avr_ul_title);
        mAvDL_title = findViewById(R.id.avr_dl_title);
        mAvUL = findViewById(R.id.ul);
        mAvDL = findViewById(R.id.dl);
        mGeo_title = findViewById(R.id.geo_ref_title);
        mGeo = findViewById(R.id.geo_ref);
        line1 = findViewById(R.id.line);
        line2 = findViewById(R.id.line2);

        startTitle = findViewById(R.id.start_title);

        //Init views vector
        /* vecs array are faster than maps? */
        mButtons = new View[8];
        mViews = new View[25];
        mDual = new View[2];
        mViews[0] = mFreq;
        mViews[1] = mFreq_title;
        mViews[2] = mCell;
        mViews[3] = mCell_title;
        mViews[4] = mPci;
        mViews[5] = mPci_title;
        mViews[6] = mTac;
        mViews[7] = mTac_title;
        mViews[8] = mAvDL;
        mViews[9] = mAvDL_title;
        mViews[10] = mAvUL;
        mViews[11] = mAvUL_title;
        mViews[12] = mEnb;
        mViews[13] = mEnb_title;
        mViews[14] = mLevel;
        mViews[15] = mLevel_title;
        mViews[16] = mQual;
        mViews[17] = mQual_title;
        mViews[18] = mRat;
        mViews[19] = mRat_title;
        mViews[20] = line1;
        mViews[21] = line2;
        mViews[22] = mLogo;
        mViews[23] = mGeo_title;
        mViews[24] = mGeo;

        mDual[0] = mConfirm;
        mDual[1] = mCancel;

        // mButtons[0] = mB1;
        // mButtons[1] = mB2;
        // mButtons[2] = mB3;
        // mButtons[3] = mB4;
        // mButtons[4] = mB5;
        // mButtons[5] = mB6;
        // mButtons[6] = mB7;
        mButtons[7] = mMapPath;

        // for(View str : mButtons) {
        //     str.setAlpha(0);
        // }

        //Speed gauge
        // change MAX speed to 320
        speedView = (SpeedView) findViewById(R.id.speedView);
        speedView.setMaxSpeed(320);
        speedView.setTickNumber(5);
        speedView.speedTo(0);
        speedView.setAlpha(0f);


        //check service status

        checkService();

        turnReadingOff();
        hideButtonForTest();

        //Telephony manager...
        //TODO : move to service!


        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        initChart();

        Log.d("jojo","autopress"+TestService.screen);
        if(TestService.screen==0 && TestService.isAppending==false) {
            autoPress();
        }

    }


    public void clickItem(int index) {
        switch(index) {
            case 0 :
                selectTest(1);
                break;
            case 1 :
                selectTest(2);
                break;

            case 2 :
                selectTest(3);
                break;

            case 3 :
                selectTest(4);
                break;

            case 4 :
                selectTest(5);
                break;
            case 5 :
                selectTest(6);
                break;

            case 6 : selectTest(7);
                break;
        }
    }

    public void selectTest(int test) {

        if(test == 1) {
            Log.d("mb1", TestService.screen + "");
            if (TestService.screen == 1 && lock == false) {

                boolean gps_enabled = false;

                try {
                    gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }

                if (gps_enabled) {
                    //seelct test idle
                    TestService.testType = 1;
                    lock = true;
                    TestService.screen = 2;
                    hideTestSelects(0);
                } else {
                    Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                }

            }
        }

        if(test==2){
            {
                if(TestService.screen==1 && lock == false){

                    boolean gps_enabled = false;

                    try {
                        gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    } catch(Exception ex) {}

                    if(gps_enabled) {
                        //seelct test data dl
                        TestService.testType = 2;
                        lock = true;
                        TestService.screen = 2;
                        hideTestSelects(1);
                    }

                    else{
                        Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }


        if(test==3){
            if(TestService.screen==1 && lock == false){

                boolean gps_enabled = false;

                try {
                    gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}

                if(gps_enabled) {
                    //seelct test data ul
                    TestService.testType = 3;
                    lock = true;
                    TestService.screen = 2;
                    hideTestSelects(2);
                }

                else{
                    Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                }
            }
        }


        if(test==4){
            if(TestService.screen==1 && lock == false){

                boolean gps_enabled = false;

                try {
                    gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}


                if(gps_enabled) {
                    //seelct test speedtest
                    TestService.testType = 4;
                    lock = true;
                    TestService.screen = 2;
                    hideTestSelects(3);
                }

                else{
                    Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if(test==5){
            if(TestService.screen==1 && lock == false){

                boolean gps_enabled = false;

                try {
                    gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}

                if(gps_enabled) {
                    //seelct test drop
                    TestService.testType = 5;
                    lock = true;
                    TestService.screen = 2;
                    hideTestSelects(4);
                }

                else{
                    Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                }
            }
        }


        if(test==6){
            if(TestService.screen==1 && lock == false){

                boolean gps_enabled = false;

                try {
                    gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}

                if(gps_enabled) {
                    //seelct test data ul
                    TestService.testType = 6;
                    lock = true;
                    TestService.screen = 2;
                    hideTestSelects(5);
                }

                else{
                    Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                }
            }
        }


        if(test==7){
            if(TestService.screen==1 && lock == false){

                boolean gps_enabled = false;

                try {
                    gps_enabled = TestService.lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}

                if(gps_enabled) {
                    //seelct test data ul
                    TestService.testType = 7;
                    lock = true;
                    TestService.screen = 2;
                    hideTestSelects(6);
                }

                else{
                    Toast.makeText(getApplicationContext(), "Please, you have to activate GPS before starting a test!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    public Runnable loginError = new Runnable() {
        @Override
        public void run() {

            loginSet.setAlpha(1f);
            loginMail.setAlpha(1f);
            loginPass.setAlpha(1f);

            loginSet.setBackground(getResources().getDrawable(R.drawable.squareconfirmwhite));
            ImageView icon = findViewById(R.id.iconLogin);
            icon.setBackground(getResources().getDrawable(R.drawable.err));
            displayLogin.setText("Invalid password/user or network error!");
            loginMail.setBackground(getResources().getDrawable(R.drawable.squareconfirmred));
            loginPass.setBackground(getResources().getDrawable(R.drawable.squareconfirmred));

        }
    };

    public Runnable goToMain = new Runnable() {
        @Override
        public void run() {
            TestService.shouldSkip = true;
            goToMainActivity();
        }
    };

    public void autoPress() {
        lock = true;
        turnReadingOff();
        isReadActive = false;
        prepareToSelectTest();
        TestService.screen = 1;
    }

    public void prepareToSending() {
        shouldUpdateIcon = true;
        testFx = false;
        mStop.changeBackgroundToNC();
        mStop.setEnabled(false);
        turnReadingOff();
        isReadActive = false;
        displayState.setText("Sending results...");

        TestService.stopAppending();
    }


    public void hideMasterButton(){
        //hide and disable BIG blue button..
        pendingTest.setEnabled(false);
        pendingTest.setTranslationY(dpToPx(-900,getApplicationContext()));
        //------------------
    }

    public void prepareToSelectTest() {

        //..go to selection test screen
        TestService.screen = 1;
        completed = false;

        mMapPath.setEnabled(false);
        ObjectAnimator animation3 = ObjectAnimator.ofFloat(mMapPath, "alpha",0);
        animation3.setDuration(400);//(900);
        animation3.start();

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                stepbar.setImageDrawable(getResources().getDrawable(R.drawable.stepbarf3));
                displayState.setText("Please, select a test");

                //hide and disable BIG blue button..
                pendingTest.setEnabled(false);
                pendingTest.setTranslationY(dpToPx(-900,getApplicationContext()));

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(menu, "alpha",1);
                animation2.setDuration(300);
                animation2.start();
                menu.setEnabled(true);

                //------------------

                /*
                for(int n=0; n<7; n++) {

                    mButtons[n].setEnabled(true);
                    mButtons[n].setTranslationY(dpToPx(0,getApplicationContext()));
                    mButtons[n].setTranslationX(dpToPx(-120,getApplicationContext()));

                    ObjectAnimator animation2 = ObjectAnimator.ofFloat(mButtons[n], "translationX", dpToPx(0, getApplicationContext()));
                    animation2.setDuration(900+n*100);
                    animation2.start();



                    ObjectAnimator animation = ObjectAnimator.ofFloat(mButtons[n], "alpha", 1);
                    animation.setDuration(900+n*100);
                    animation.start();


                    handler.removeCallbacksAndMessages(null);
                }
*/

            }
        };

        handler.postDelayed(runnable,200);



        final Handler handler2 = new Handler();

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                lock = false;
                TestService.screen = 1;
                handler2.removeCallbacksAndMessages(null);
            }
        };

        handler2.postDelayed(runnable2,1200);

    }



    public static int dpToPx(float dpValue, Context context){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, context.getResources().getDisplayMetrics());
    }


    public void turnReadingOff() {

        archtoogle.setText("show info");

        ObjectAnimator animation = ObjectAnimator.ofFloat(archtoogle, "translationY", dpToPx(-200,this));
        animation.setDuration(600);
        animation.start();


        ObjectAnimator animation2 = ObjectAnimator.ofFloat(archdown, "translationY", dpToPx(-200,this));
        animation2.setDuration(600);
        animation2.start();


        ObjectAnimator animation3 = ObjectAnimator.ofFloat(chart, "translationY", dpToPx(-200,this));
        animation3.setDuration(600);
        animation3.start();


        for(int n=0; n<25; n++) {
            ObjectAnimator animationN = ObjectAnimator.ofFloat(mViews[n], "translationY", dpToPx(-200, this));
            animationN.setDuration(600);
            animationN.start();

        }


    }



    public void turnReadingOn() {

        archtoogle.setText("hide info");

        ObjectAnimator animation = ObjectAnimator.ofFloat(archtoogle, "translationY", dpToPx(0,this));
        animation.setDuration(600);
        animation.start();

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(archdown, "translationY", dpToPx(0,this));
        animation2.setDuration(600);
        animation2.start();

        ObjectAnimator animation3 = ObjectAnimator.ofFloat(chart, "translationY", dpToPx(0,this));
        animation3.setDuration(600);
        animation3.start();

        for(int n=0; n<25; n++) {
            ObjectAnimator animationN = ObjectAnimator.ofFloat(mViews[n], "translationY", dpToPx(0, this));
            animationN.setDuration(600);
            animationN.start();
        }
    }


    public void initChart() {
        chart = findViewById(R.id.chart1);
        chart.setDrawGridBackground(false);
        chart.setTouchEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.setNoDataText("");

        disableCharAxis(chart);

        for(int u=0; u<20; u++){
            values.add(new Entry(values.size() + 1, 100f, getResources().getDrawable(R.drawable.star)));
        }

    }


    public void disableCharAxis(LineChart chart) {
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisLeft().setDrawTopYLabelEntry(false);
        chart.getAxisLeft().setDrawGridLinesBehindData(false);
        chart.getAxisLeft().setDrawLabels(false);

        chart.getAxisRight().setDrawGridLines(false);
        chart.getAxisRight().setDrawTopYLabelEntry(false);
        chart.getAxisRight().setDrawGridLinesBehindData(false);
        chart.getAxisRight().setDrawLabels(false);

        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().setDrawLabels(false);
        chart.getXAxis().setDrawGridLines(false);
    }



    private void setData(int value) {


        if(values.size()<20) {
            values.add(new Entry(values.size() + 1, (float) value, getResources().getDrawable(R.drawable.star)));
        }
        else{
            for(int i = 0; i<19; i++) {
                values.get(i+1).setX(i);
                values.set(i, values.get(i + 1));
                Log.d("graph",values.get(i)+"");
            }
            // values.set(0,values.get(1));
            values.set(19, new Entry(20,(float) value, getResources().getDrawable(R.drawable.star)));
        }


        LineDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.notifyDataSetChanged();
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");

            set1.setDrawIcons(false);


            // draw dashed line
            set1.enableDashedLine(0f, 0f, 0f);

            // black lines and points
            set1.setColor(getResources().getColor(R.color.colorLine));
            set1.setDrawCircleHole(false);
            set1.setDrawCircles(false);


            // line thickness and point size
            set1.setLineWidth(2f);
            set1.setCircleRadius(3f);

            // draw points as solid circles
            set1.setDrawCircleHole(false);

            // text size of values
            set1.setValueTextSize(0f);

            // draw selection line as dashed
            set1.enableDashedHighlightLine(0f, 5f, 0f);

            // set the filled area
            set1.setDrawFilled(true);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return chart.getAxisLeft().getAxisMinimum();
                }
            });

            // set color of filled area
            if (Utils.getSDKInt() >= 18) {
                // drawables only supported on api level 18 and above
                Drawable drawable;
                //  Log.d("coooolor",value+"");
                drawable = ContextCompat.getDrawable(this, R.drawable.fade_white);
                // else drawable = ContextCompat.getDrawable(this, R.drawable.fade_white);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.WHITE);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the data sets

            // create a data object with the data sets
            LineData data = new LineData(dataSets);

            // set data
            chart.setData(data);
        }


    }



    public void startReading() {
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                goforReading();

                if (isReadActive) handler.postDelayed(this, 1000);
                else handler.removeCallbacksAndMessages(null);
            }
        };

        if (isReadActive) handler.postDelayed(runnable, 1000);

    }


    public void checkLoginResponse(){
        TestService.pass = pass;
        TestService.mail = mail;
        initLoginAttemp();
    }

    public void startFx() {
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                if(!goAlpha){
                    if(alpha>0) alpha = alpha - 0.080f;
                    else goAlpha = true;
                }
                else {
                    if(alpha<1) alpha = alpha + 0.080f;
                    else goAlpha = false;
                }


                if (testFx) handler.postDelayed(this, 50);
                else handler.removeCallbacksAndMessages(null);
            }
        };

        if (testFx) handler.postDelayed(runnable, 50);

    }

    public void setPtrToMap(Map<String,String> hashMap) {
        georefM = hashMap;
    }


    public void nowError() {
        runOnUiThread(errorMex);
    }

    public void unsubNotM() {
        runOnUiThread(unsubOK);
    }

    public void unsubNotMN() {
        runOnUiThread(unsubNot);
    }

    public void nowResuming() {
        final Handler handler2 = new Handler();

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {


                if(testTypeUI==2){
                    isReadActive = true;
                    turnReadingOn();
                    startReading();
                    showGauge();
                    showCloud();
                }

                if(testTypeUI==3){
                    isReadActive = true;
                    turnReadingOn();
                    startReading();
                    showGauge();
                    showCloudButNotClickable();
                }


                if(testTypeUI==4){
                    isReadActive = true;
                    turnReadingOn();
                    startReading();
                    showGauge();
                    showCloudButNotClickable();
                }

                if(testTypeUI==1 || testTypeUI==5 || testTypeUI==6) {
                    Log.d("hylo","sdsd");
                    isReadActive = true;
                    turnReadingOn();
                    startReading();
                    showCloud();
                }

                handler2.removeCallbacksAndMessages(null);

            }
        };

        handler2.postDelayed(runnable2,500);

        displayState.setText("Now Testing : "+label);

    }

    public void nowTesting() {
        displayState.setText("Now Testing : "+label);

        if(testTypeUI==2 || testTypeUI==7){
            if(isReadActive==false) {
                isReadActive = true;
                turnReadingOn();
                startReading();
            }
            showGauge();
            showCloud();
        }


        if(testTypeUI==3){
            if(isReadActive==false) {
                isReadActive = true;
                turnReadingOn();
                startReading();
            }
            showGauge();
            showCloudButNotClickable();
        }


        if(testTypeUI==4){
            if(isReadActive==false) {
                isReadActive = true;
                turnReadingOn();
                startReading();
            }
            showGauge();
            showCloudButNotClickable();
        }

        if(testTypeUI==1 || testTypeUI==5 || testTypeUI==6) {
            if(isReadActive==false) {
                isReadActive = true;
                turnReadingOn();
                startReading();
            }
            showCloud();
        }

        TestService.mail = mail;
        Log.d("wow", mail+ "ashdasd");
        TestService.testType = testTypeUI;
        TestService.resetForNewTest();
        TestService.startAppending();
    }


    public void showGauge() {
        ObjectAnimator animation = ObjectAnimator.ofFloat(speedView, "alpha", 1);
        animation.setDuration(300);
        animation.start();
    }

    public void hideGauge() {
        ObjectAnimator animation = ObjectAnimator.ofFloat(speedView, "alpha",0);
        animation.setDuration(300);
        animation.start();
    }


    public void nowFinished() {
        shouldUpdateIcon = true;
        runOnUiThread(successMex);
    }


    public void hideTestSelects(int button) {

        final int buttonF = button;
        label = "";

        switch(buttonF) {
            case 0 : label = "IDLE"; testTypeUI = 1; break;
            case 1 : label = "DATA DL"; testTypeUI = 2; break;
            case 2 : label = "DATA UP"; testTypeUI = 3; break;
            case 3 : label = "SPEED"; testTypeUI = 4; break;
            case 4 : label = "CALL"; testTypeUI = 5; break;
            case 5 : label = "PING"; testTypeUI = 6; break;
            case 6 : label = "MULTIRAB"; testTypeUI = 7; break;
        }

        final String labelF = label;

        /*
        for(int n=0; n<7; n++){
            if(n!=button){
                ObjectAnimator animation = ObjectAnimator.ofFloat(mButtons[n], "alpha",0);
                animation.setDuration(300);
                animation.start();
            }
        }
*/
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(menu, "alpha",0);
        animation2.setDuration(300);
        animation2.start();


        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                stepbar.setImageDrawable(getResources().getDrawable(R.drawable.stepbarf4));
                displayState.setText("Confirm starting "+labelF+" test?");
                lock = false;

                /*
                ObjectAnimator animation = ObjectAnimator.ofFloat(mButtons[buttonF], "alpha",0);
                animation.setDuration(300);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(mButtons[buttonF], "translationY",dpToPx(-100,getApplicationContext()));
                animation2.setDuration(400);
                animation2.start();
                */

                ObjectAnimator animation3 = ObjectAnimator.ofFloat(mConfirm, "alpha",1);
                animation3.setDuration(400);
                animation3.start();

                ObjectAnimator animation4 = ObjectAnimator.ofFloat(mCancel, "alpha",1);
                animation4.setDuration(400);
                animation4.start();

                handler.removeCallbacksAndMessages(null);

            }
        };

        handler.postDelayed(runnable,400);



        final Handler handler2 = new Handler();

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {

                /*
                for(int i=0; i<5; i++) {
                    mButtons[i].setTranslationY(dpToPx(-900, getApplicationContext()));
                }
                */

                mConfirm.setTranslationY(dpToPx(0,getApplicationContext()));
                mCancel.setTranslationY(dpToPx(0,getApplicationContext()));

                mConfirm.setEnabled(true);
                mCancel.setEnabled(true);

                handler2.removeCallbacksAndMessages(null);

            }
        };

        handler2.postDelayed(runnable2,500);

    }

    public void hideConfirmCancel(int button){

        final int buttonF = button;

        for(int n=0; n<2; n++){
            if(n!=button){
                ObjectAnimator animation = ObjectAnimator.ofFloat(mDual[n], "alpha",0);
                animation.setDuration(400);
                animation.start();
            }
        }


        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                ObjectAnimator animation = ObjectAnimator.ofFloat(mDual[buttonF], "alpha",0);
                animation.setDuration(400);
                animation.start();

                ObjectAnimator animation2 = ObjectAnimator.ofFloat(mDual[buttonF], "translationY",dpToPx(-100,getApplicationContext()));
                animation2.setDuration(400);
                animation2.start();

                handler.removeCallbacksAndMessages(null);

            }
        };

        handler.postDelayed(runnable,400);



        final Handler handler2 = new Handler();

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {

                for(int i=0; i<2; i++){
                    mDual[i].setEnabled(false);
                    mDual[i].setTranslationY(dpToPx(-900,getApplicationContext()));
                }

                if(buttonF==1) prepareToSelectTest();
                else nowTesting();

                handler2.removeCallbacksAndMessages(null);

            }

        };

        handler2.postDelayed(runnable2,900);

    }

    public Runnable errorMex = new Runnable() {
        @Override
        public void run() {
            mStop.setEnabled(true);
            displayState.setText("No Data Connection!");
            // cloudLoading2.setTranslationY(dpToPx(-900,getApplicationContext()));
            mStop.changeBackgroundToError();
        }
    };


    public Runnable unsubOK = new Runnable() {
        @Override
        public void run() {
            kill();
        }
    };

    public Runnable unsubNot = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), "Network error, please retry.", Toast.LENGTH_SHORT).show();
        }
    };


    Runnable successMex = new Runnable() {
        @Override
        public void run() {
            mStop.setEnabled(true);

            mStop.changeBackgroundToCompleted();
            stepbar.setImageDrawable(getResources().getDrawable(R.drawable.stepbarf5));
            //set average level in UI
            mAvUL.setText(TestService.lastULavr + " kb/s");
            mAvDL.setText(TestService.lastDLavr + " kb/s");

            final Handler handler = new Handler();

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if(shouldUpdateIcon)
                        mStop.changeBackgroundToCompletedF();

                    handler.removeCallbacksAndMessages(null);
                }
            };

            handler.postDelayed(runnable,3200);


            //enable path button
            mMapPath.setEnabled(true);
            ObjectAnimator animation3 = ObjectAnimator.ofFloat(mMapPath, "alpha",1);
            animation3.setDuration(400);//(900);
            animation3.start();
            //--------------


            if(testTypeUI==1) {
                displayState.setText("Test "+TestService.sessionId+" : IDLE\nAverage Level : "+ TestService.avrLevelForIDLE+" dBm");
            }

            else if(testTypeUI==2){
                displayState.setText("Test "+TestService.sessionId+" : DATA DL\nAverage DL : "+ TestService.lastDLavr+" kb/s\n");
            }

            else if(testTypeUI==3){
                displayState.setText("Test "+TestService.sessionId+" : DATA UL\nAverage UL : "+ TestService.lastULavr+" kb/s\n");
            }

            else if(testTypeUI==4){
                displayState.setText("Test "+TestService.sessionId+" : SPEEDTEST\nAverage DL : "+ TestService.lastDLavr+" kb/s\n"+
                        "Average UL : "+TestService.lastULavr + " kb/s\n" + "Ping : " + TestService.lastPing + " ms");
            }

            else if(testTypeUI==5){
                displayState.setText("Test "+TestService.sessionId+" : CALL DROP\nDropped calls : "+ TestService.lastDropped+"\n");
            }

            else if(testTypeUI==6){
                displayState.setText("Test "+TestService.sessionId+" : PING\nAverage ping : "+ TestService.pingAverage+  " ms"+"\n");
            }


            else if(testTypeUI==7){
                displayState.setText("Test "+TestService.sessionId+" : MULTIRAB\nDropped calls : "+ TestService.lastDropped+"\n"+ TestService.lastDLavr+" kb/s");
            }


            else
                displayState.setText("Test Completed!");

            completed = true;

            //send up the logger!
            turnReadingOff();
            isReadActive=false;

        }
    };


    public void hideButtonForTest() {

        //   for(int n = 0; n<6; n++){

        //       mButtons[n].setAlpha(0f);
        //        mButtons[n].setEnabled(false);
        //        }

        mConfirm.setTranslationY(dpToPx(-900,this)); //-900 os-code
        mCancel.setTranslationY(dpToPx(-900,this));

        mConfirm.setAlpha(0f);
        mCancel.setAlpha(0f);


        mConfirm.setEnabled(false);
        mCancel.setEnabled(false);

        hideCloudButton();

    }


    public void hideCloudButton() {
        mStop.setEnabled(false);
        mStop.setAlpha(0);
        mStop.setTranslationY(dpToPx(-900,getApplicationContext()));

        testFx = false;
    }

    public void finishedResetButton() {

        ObjectAnimator animation = ObjectAnimator.ofFloat(mStop, "translationY", dpToPx(-100,this));
        animation.setDuration(400);
        animation.start();

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(mStop, "alpha",0);
        animation2.setDuration(300);
        animation2.start();

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                hideCloudButton();
                mStop.changeBackgroundToBump();
                handler.removeCallbacksAndMessages(null);
            }
        };

        handler.postDelayed(runnable,500);
    }

    public void showCloud() {
        mStop.setEnabled(true);

        if(testTypeUI!=2 && testTypeUI!=4 && testTypeUI!=3 && testTypeUI!=7) {
            // cloudLoading2.setTranslationY(dpToPx(0, getApplicationContext()));
            mStop.setTranslationY(dpToPx(0, getApplicationContext()));
        }
        else {
            // cloudLoading2.setTranslationY(dpToPx(200, getApplicationContext()));
            mStop.setTranslationY(dpToPx(200, getApplicationContext()));
        }


        ObjectAnimator animation = ObjectAnimator.ofFloat(mStop, "alpha",1);
        animation.setDuration(600);
        animation.start();

        testFx = true;
        startFx();
    }


    public void showCloudButNotClickable() {
        mStop.setEnabled(false);
        if(testTypeUI!=2 && testTypeUI!=4 && testTypeUI!=3) {
            mStop.setTranslationY(dpToPx(0, getApplicationContext()));
        }
        else {
            mStop.setTranslationY(dpToPx(200, getApplicationContext()));
        }
        mStop.changeBackgroundToAUTO();

        ObjectAnimator animation = ObjectAnimator.ofFloat(mStop, "alpha",1);
        animation.setDuration(600);
        animation.start();

        testFx = true;
        startFx();
    }

    public TelephonyManager getTm(){
        return tm;
    }


    public void goforReading() {

        /*Cache lac from service!*/

        //start task for retriving geo-ref list
        if(cachedLac!=null && !cachedLac.equals(TestService.tac)) {
            cachedLac = TestService.tac+"";
            initCellRefList();
        }

        Log.d("SERVICE","status :"+isReadActive);
        //setting the titles based on RAT , updating in rt.

        switch(TestService.rat)
        {
            case "4" : {
                mPci_title.setText("PCI");
                mEnb_title.setText("eNodeB");
                mCell_title.setText("CI");
                break;
            }

            case "3" : {
                mPci_title.setText("PSC");
                mEnb_title.setText("RNC ");
                mCell_title.setText("Cell id");
                break;
            }

            case "2" : {
                mPci_title.setText("BSIC");
                mEnb_title.setText("NODEB ");

                break;
            }
        }

        /*
        Log.d ("ren_nr",TestService.registerStatus);
        if (TestService.logType == "IDLE_LOG") {
            Log.d("ren_nr",TestService.logType);*/

        mFreq.setText(TestService.freq+"");
        mPci5G.setText(TestService.pciUI+"");
        mPci5G_title.setText("Pci5G");
        mCell.setText(TestService.ci+"");
        mPci.setText(TestService.pci+"");
        mTac.setText(TestService.tac+"");
        mEnb.setText(TestService.enb+"");
       // mrsrp35g.setText(TestService.rsrp3UI+"");




    if(!TestService.rsrp3.equals("")){

            if (Integer.valueOf(TestService.rsrp3) < -108 ) {
                mLevel.setTextColor(getResources().getColor(R.color.colorRed));
            } else if (Integer.valueOf(TestService.rsrp3) < -98) {
                mLevel.setTextColor(getResources().getColor(R.color.colorYel));
            } else {
                mLevel.setTextColor(getResources().getColor(R.color.colorGreen));
            }

        }

        mLevel.setText(TestService.rsrp3+"");
        mQual.setText(TestService.rsrq3+"");
        mRat.setText(TestService.rat+"");


        if(georefM!=null){
            String buffer = "";

            if(TestService.rat.equals("4")) {
                buffer = georefM.get(TestService.pci + TestService.ci);
            }
            else {
                buffer = georefM.get(TestService.ci);
            }

            if(buffer==null) buffer = "nd";
            mGeo.setText(buffer+"");
        }

        runOnUiThread(settingData);
    }




    private Runnable settingData = new Runnable() {
        @Override
        public void run() {
            if(!TestService.rsrp3.equals("")) {
                setData(Integer.valueOf(TestService.rsrp3) + 140);
                chart.animateX(400);
            }
        }
    };




    public void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.SEND_SMS,Manifest.permission.READ_PHONE_STATE,Manifest.permission.CALL_PHONE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 5
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ) {
                    initSaveFile();
                    if(pref.getString("disclaimer","nothing").equals("nothing")){
                        //we we're using registration..now jump to disclaimer
                        goToDisclaimer();
                    }
                    else{
                        mail = pref.getString("email","nothing");
                        Log.d("pref","problem"+mail);
                        //goToMainActivity();..go to login, instead.
                        shouldSkinAuth = TestService.shouldSkip;
                        if(shouldSkinAuth) {
                            goToMainActivity();
                        } else goToLogin();
                    }

                } else {
                    requestPermission();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    public void checkService(){
        if (!isMyServiceRunning(TestService.class)) {

            Intent i = new Intent(getApplicationContext(), TestService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getApplicationContext().startForegroundService(i);
            }

            else getApplicationContext().startService(i);

            Log.d("SERVICE", "Servizio avviato da MainActivity");
        }


        else { TestService.ptr = this; shouldSkinAuth = true; lifeSaver(); return; }
        //init masterpointer
        TestService.ptr = this;

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(getApplicationContext().ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public void exitAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage("Do you want to exit and close application? Be careful to NOT EXIT white undertest! (ATTENTION : it will NOT be running in background)")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                kill();
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        navigation.setSelectedItemId(R.id.action_item1);
                        dialog.cancel();
                    }
                });
        alertDialogBuilder.show();
    }


    public void unsubAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage("Do you want to UNSUBSCRIBE your account? This action cannot be undone. (application will be closed)")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                initUnsubAttemp();
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){

                        dialog.cancel();
                    }
                });
        alertDialogBuilder.show();
    }

//TODO : rule of three, refactor!

    public void initCellRefList(){
        //attemp georeflist...
        TaskOkHttp connectionSocket;
        connectionSocket = new TaskOkHttp();
        connectionSocket.setType(4);
        connectionSocket.initVarsGeoRef(this);
        connectionSocket.start();
    }


    public void initUnsubAttemp(){
        //attemp unsub...
        TaskOkHttp connectionSocket;
        connectionSocket = new TaskOkHttp();
        connectionSocket.setType(3);
        connectionSocket.initVarsUns(this);
        connectionSocket.start();
    }


    public void initLoginAttemp() {
        //attemp login...
        TaskOkHttp connectionSocket;
        connectionSocket = new TaskOkHttp();
        connectionSocket.setType(2);
        connectionSocket.initVarsLogin(this);
        connectionSocket.start();
    }

    public void kill(){

        isReadActive = false;
        TestService.stop();

        stopService(new Intent(this,TestService.class));

        finishAndRemoveTask();

    }


    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Press EXIT button to leave application", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        TestService.dlqueueregistered = false;

    }





}
