package net.wind3.youonetfork.task;

import android.util.Log;


import net.wind3.youonetfork.MainActivity;
import net.wind3.youonetfork.services.TestService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TaskOkHttp extends Thread {

    MainActivity ptrM;
    protected String myImei = "ND";
    protected String type = "";

    protected final String SERVER_URL= "https://www.t-connect.cloud/automation/logs.php";
    protected final String LOGIN_URL = "https://www.t-connect.cloud/automation/yon_login.php";
    protected final String GEOREF_URL = "https://www.t-connect.cloud/automation/getcellbylac.php?lac=";

    //hashmap for georef
    private Map<String, String> georef = new HashMap<>();

    /* Hackish but our client is changing features every day lol */

    String logfilepath = "";
    String mail = "";
    String password = "";

    String dropped = "";
    String level = "";
    String avrDL = "";
    String avrUP = "";
    String avrPing = "";
    String maxSpeedDL = "";
    String maxSpeedUL = "";


    int countRetry = 0;

    int typeTask = 0;


    public void initVars(MainActivity ptr, String imei, String filepath){
        logfilepath = filepath;
        myImei = imei;
        ptrM = ptr;
    }

    public void initVarsGeoRef(MainActivity ptr){ ptrM = ptr; }

    public void initVarsUns(MainActivity ptr) { ptrM = ptr; }

    public void initVarsLogin(MainActivity ptr){
        ptrM = ptr;
    }

    public void setType(int par){
        typeTask = par;
    }

    @Override
    public void run() {

        switch(typeTask) {

            case 0 : break;


            case 1 : {
                initLog();
                typeTask = 0;
                break;
            }

            case 2 : {
                initLogin();
                typeTask = 0;
                break;
            }

            case 3 : {
                initUnsub();
                typeTask = 0;
                break;
            }

            case 4 : {
                initGeoRef();
                typeTask = 0;
                break;
            }

        }



    }



    public void initLog() {
        getVarsFromService();
        filterForType();
        getLogFilePath();
        sendLog();
    }

    public void initLogin(){
        mail = TestService.mail+"";
        password = TestService.pass+"";
        sendLogin();
    }

    public void initUnsub(){
        unsubscribeMe();
    }

    public void initGeoRef(){
        sendParamsForGeo();
    }

    public void getVarsFromService() {

        mail = TestService.mail+"";

        dropped = TestService.dropped+"";

        if(TestService.testType==2) {
            avrDL = TestService.dlAvr + "";
            Log.d("cazzo", TestService.dlAvr+"");
        }
        else {
            avrDL = TestService.speedSTdl + "";
        }

        avrUP = TestService.speedSTup+"";

        if(TestService.testType!=6) {
            avrPing = TestService.pingST + "";
        }
        else {
            avrPing = TestService.pingAverage;
        }
        
        maxSpeedDL = TestService.maxSpeedDL+"";
        maxSpeedUL = TestService.maxSpeedUL+"";

        //set average levels in UI

        Log.d("levels",TestService.idleSum+"");
        Log.d("levels",TestService.counterIdle+" "+(-1*(TestService.idleSum/TestService.counterIdle)));

        double avr = TestService.idleSum/TestService.counterIdle;

        Log.d("levelidle",TestService.idleSum+"");
        avr = 10*Math.log10(avr);

        level = avr+"";
        level = Math.round(Float.valueOf(level))+"";

        Log.d("levels",level);
        }


    public void filterForType() {
        if(TestService.testType == 1) {
            type = "IDLE";
        }

        if(TestService.testType==2) {
            type = "DATA_DL";
        }

        if(TestService.testType==3) {
            type = "DATA_UP";
        }

        if(TestService.testType==4) {
            type = "SPEEDTEST";
        }

        if(TestService.testType==5) {
            type = "DROP_CALL";
        }

        if(TestService.testType==6){
            type= "PING";
        }

        if(TestService.testType==7){
            type= "MULTIRAB";
        }
    }

    public void getLogFilePath() {
        logfilepath = TestService.file;
    }


    protected String unsubscribeMe() {


        Log.d("unsubscribe","sdsd");
        OkHttpClient client = new OkHttpClient();


        Request request = new Request.Builder()
                .url("https://www.u2y.eu/automation/sendmail_unsubscribe.php?mail="+TestService.mail)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                //ok
                Log.d("unsubscribe","unsub");
                ptrM.unsubNotM();
            }

            else {
                Log.d("unsubscribe","not");
                ptrM.unsubNotMN();
            }

        } catch (Exception e) {
            ptrM.unsubNotMN();
            Log.d("unsubscribe","error");
        }

        //typeTask = 0;
    return "ok";
    }






    protected void sendLog() {

        File fileToUpload = new File(ptrM.getApplicationContext().getExternalFilesDir(null), logfilepath);

        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
         .addFormDataPart("email", mail)
         .addFormDataPart("imei", myImei+"")
         .addFormDataPart("average_dl", avrDL)
         .addFormDataPart("average_ul", avrUP)
         .addFormDataPart("average_ping", avrPing)
         .addFormDataPart("average_dBm", level)
         .addFormDataPart("dropped_calls", dropped)
         .addFormDataPart("session_type",type)
         .addFormDataPart("max_speed_dl", maxSpeedDL)
         .addFormDataPart("max_speed_ul", maxSpeedUL)

         .addFormDataPart("log", "log", RequestBody.create(MediaType.parse("application/octet-stream"),
                fileToUpload))

        .build();

        Request request = null;
        try {
          request = new Request.Builder()
                  .url(SERVER_URL)
                  .post(requestBody)
                  .build();
      }

      catch(Exception e){

      }

        try {
        Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                doMathForUI();
                JSONObject json = new JSONObject(response.body().string());
                TestService.urlMap = json.getString("link")+"&openMap=true";
                TestService.sessionId = json.getString("session_id");
                Log.d("hypersux",TestService.urlMap);
                ptrM.nowFinished();
                TestService.resetForNewTest();
                }

                else {
                Log.d("LOG",response.toString());
                ptrM.nowError();
            }

        } catch (Exception e) {
        e.printStackTrace();
        Log.d("LOG","file not sended");
        ptrM.nowError();
        }

        //typeTask = 0;
         }

         public void doMathForUI() {
             double avr = TestService.idleSum/TestService.counterIdle;

             avr = 10*Math.log10(avr);

             TestService.avrLevelForIDLE = avr+"";
             TestService.avrLevelForIDLE = Math.round(Float.valueOf(level))+"";
         }


    protected void sendParamsForGeo() {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(GEOREF_URL+TestService.tac)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                //200 server code
                JSONArray jsonArray = new JSONArray(response.body().string());
                for(int i=0; i<jsonArray.length(); i++){
                   JSONObject json = jsonArray.getJSONObject(i);
                   String bufferCell = json.getString("cell");

                   //placeholder for values
                   String pci = json.getString("PCI");
                   String ci = json.getString("CI");
                   //mapping refs.
                   Log.d("GEOREF","ok!!");
                   georef.put(pci+ci,bufferCell);
                   ptrM.setPtrToMap(georef);
                }
            }

            else {
                Log.d("GEOREF",response.toString()+ "");
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("GEOREF","GEOREF : msg -> network exception catched");
            return;
        }

    }


    protected void sendLogin() {

        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("email", mail)
                .addFormDataPart("pass", password)

                .build();

        Request request = null;
        try {
            request = new Request.Builder()
                    .url(LOGIN_URL)
                    .post(requestBody)
                    .build();
        }

        catch(Exception e){
          //dummy
        }

        try {

            Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                //ok
                JSONObject json = new JSONObject(response.body().string());
                TestService.token = json.getString("token");
                Log.d("login",TestService.token+"");
                ptrM.runOnUiThread(ptrM.goToMain);

            }

            else {
                Log.d("LOGIN",response.toString());
                ptrM.runOnUiThread(ptrM.loginError);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            ptrM.runOnUiThread(ptrM.loginError);
            return;
        }

    }


     }

